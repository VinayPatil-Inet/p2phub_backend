var routes = require('express')();
const organization = require('./controllers/organization');
const users = require('./controllers/user');
const contractdetails = require('./controllers/contract_details');
const payercontact = require('./controllers/payer_contact');
const usertypes = require('./controllers/user_types');
const PayersSignedStatus = require('./controllers/payers_signed_status')
const EndPoints = require('./controllers/end_points')
var upload = require('./config/upload')
const verifyToken = require('./controllers/jwt-helpers');
var adapter = require('./controllers/adapter')




//Organization
routes.get('/organization/get/:payername/:location/:payerid/:organizationtype', organization.Organisation)
routes.post('/organization', organization.createOrganisation)
routes.put('/organization/:id/:stateid', organization.updateOrganisation)
routes.delete('/organization/:id', organization.deleteOrganisation)
routes.get('/organization/getOrganisationByUserId/:id', organization.getOrganisationByUserId)
routes.get('/organization/getOrganisationByViewId/:value', organization.getOrganisationId)
routes.get('/organization/getAdminOrganisationByViewId/:value', organization.getAdminOrganisationId)
routes.get('/getOrganisationAddMyList/:value', organization.getOrganisationAddMyList)
routes.put('/updateImageProfile/:value', upload.single('file'), organization.updateImageProfile)
routes.post('/createOrganizationdetails', organization.createOrganizationdetails)
routes.post('/create/admin/Organizationdetails', organization.createAdminOrganizationdetails)
routes.get('/organization/getBothPayers/:statusid', organization.getBothPayers)
routes.get('/getStates', organization.getStates)
routes.get('/getOrganisationTypes', organization.getOrganisationTypes)
routes.get('/getAllOrganisation', organization.getAllOrganisation)
routes.put('/updateIsApprove/:value', organization.updateIsApprove)
routes.put('/updateIsRejected/:value', organization.updateIsRejected)
routes.get('/getEndpointCount', organization.getEndpointCount)
routes.get('/getAllAdminOrganisation', organization.getAllAdminOrganisation)
routes.put('/updateAdminPayerId/:value', organization.updateAdminPayerId)
routes.put('/updateMyOrgProfile/:value', organization.updateMyOrgProfile)
routes.get('/getAzureADToken', organization.getAzureADToken)
routes.get('/getPayersEnvironment', organization.getPayersEnvironment)
routes.get('/organization/getMyOrganizationProfile/:value', organization.getMyOrganizationProfile)
routes.get('/organization/getMyOrganizationProfileDashboard/:value', organization.getMyOrganizationProfileDashboard)
routes.post('/insertdatatopayerhub', organization.insertdatatopayerhub)
routes.get('/getAllOmegaPayers', organization.getAllOmegaPayers)
routes.get('/homeorganisationdirectory/get/:payername/:location/:organizationtype', organization.HomeOrganisationDirectory)

//Users
routes.get('/users', users.getUsers);
routes.get('/user/getIdByUser/:id', users.getIdByUser)
routes.get('/user/getEINByOrganization', users.getEINByOrganization)

routes.post('/user/create', users.createUser);
routes.put('/users/:user_id', users.updateUser);
routes.delete('/users/:id', users.deleteUser);
routes.post('/user/login', users.loginUser);
routes.post('/user/getSearchbyUser', users.getSearchbyUser)
routes.post('/user/forgotPassword', users.forgotPassword);
routes.get('/getUserOrganisationTypes', users.getOrganisationTypes)
routes.put('/user/ChangePassword/:user_id', users.ChangePassword)
routes.put('/user/ResetPassword', users.ResetPassword)
routes.get('/user/verifyEmail', users.verifyEmail);
routes.post('/user/create/AdminUser', users.createAdminUser);
routes.get('/users/get/AdminUsers', users.getAdminUsers);
routes.put('/UpdateAdminUser/:user_id', users.UpdateAdminUser);
routes.delete('/DeleteAdminUser/:user_id', users.DeleteAdminUser)

//Contract Details
routes.get('/contractdetails', contractdetails.getContractDetails);
routes.post('/contractdetails', contractdetails.createContractDetails);
routes.delete('/contractdetails/:id', contractdetails.deleteContractDetails);
routes.put('/contractdetails/:id', contractdetails.updateContractDetails);
routes.post('/createContact', contractdetails.createContact);


//Payer Contact
routes.get('/GetAllPayercontact', payercontact.getPayerContact);
routes.post('/payercontact', payercontact.createPayerContact);
routes.delete('/payercontact/:id', payercontact.deletePayerContact);
routes.put('/GetAllPayercontact/:id', payercontact.updatePayerContact);
routes.get('/getIdPayerContact/:id', payercontact.getIdPayerContact)

//User Types
routes.get('/usertypes', usertypes.getUserTypes);
routes.post('/usertypes', usertypes.createUserTypes);
routes.delete('/usertypes/:id', usertypes.deleteUserTypes);
routes.put('/usertypes/:id', usertypes.updateUserTypes);

//PayersSignedStatus
routes.get('/PayersSignedStatus', PayersSignedStatus.getPayersSignedStatus);
routes.get('/RegReqPayersSignedStatusCount/:payer_id', PayersSignedStatus.RegReqPayersSignedStatusCount);
routes.get('/RegPayersSignedStatusCount/:payer_id', PayersSignedStatus.RegPayersSignedStatusCount);
routes.post('/PayersSignedStatus/create', PayersSignedStatus.createPayersSignedStatus);
routes.post('/PayersSignedStatus/update', PayersSignedStatus.updatePayersSignedStatus);
routes.get('/RegReqPayersSignedStatus/:payer_id', PayersSignedStatus.RegReqPayersSignedStatus);
routes.get('/RegReqPayersRejectStatus/:payer_id', PayersSignedStatus.RegReqPayersRejectStatus);
routes.get('/RegReqPayersPendingStatus/:payer_id', PayersSignedStatus.RegReqPayersPendingStatus);
routes.put('/updatePayersSignedStatusAccept/:payer_signed_status_id', PayersSignedStatus.updatePayersSignedStatusAccept);
routes.put('/updatePayersSignedStatusReject/:payer_signed_status_id', PayersSignedStatus.updatePayersSignedStatusReject);
routes.get('/getAllNotifactions/:payer_id', PayersSignedStatus.getAllNotifactions);


//EndPoints
routes.get('/getEndPoints/:value', EndPoints.getEndPoints); //payerListDash(payerview Organization) endpoints get api
routes.get('/getAllEndPoints/:value', EndPoints.getAllEndPoints); //view organization  endpoints get api
routes.get('/getEndPointsAll/:user_id', EndPoints.getEndPointsAll); //login user endpoints  get api
routes.get('/getAdminEndPointsAll/:user_id', EndPoints.getAdminEndPointsAll); //view organization by user_id
routes.get('/userIdByEndpointCount/:user_id', EndPoints.userIdByEndpointCount);
routes.get('/getTypeEndPoints/:user_id/:payer_environment_type', EndPoints.getTypeEndPoints);
routes.post('/EndPoints', EndPoints.createEndPoints);
routes.post('/createAdminEndPoints', EndPoints.createAdminEndPoints);
routes.delete('/delete/EndPoints/:id', EndPoints.deleteEndPoints); //endpoints
routes.post('/update/EndPoints/:payer_end_points_id/:envType', EndPoints.updateEndPoints); //endpoints
routes.get('/getEndPointsList', EndPoints.getEndPointsList); //addendpoints and admin endpoints
routes.get('/getEndPointsUrls/:payer_id', EndPoints.getEndPointsUrls);




//Adapter
routes.post('/adapter/getMypayers', verifyToken, adapter.getMypayers)
routes.get('/adapter/getAdapterendpoint', verifyToken, adapter.getAdapterendpoint);
routes.post('/adapter/getToken', adapter.getToken);
routes.post('/adapter/getAllpayers', verifyToken, adapter.getAllpayers);


exports.routes = routes;