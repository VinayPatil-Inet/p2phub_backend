var pool = require('../config/db.config')
var bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { getAllEndPoints } = require('./end_points');
var ACCESS_TOKEN_SECRET = 'padma'



const getMypayers = async (req, res) => {
    var { payer_id, inserted_date, payer_name, last_updated_date } = req.body
    try {
        var sql = `SELECT payers_organisation_detail.payer_id FROM payers_signed_status
        LEFT JOIN payers_organisation_detail  ON payers_signed_status.destinatoin_payer_id = payers_organisation_detail.payer_id
        where payers_signed_status.payer_status ='Signed' and  payers_signed_status.source_payer_id =${payer_id}`

        if (last_updated_date != undefined) {
            sql = sql + ` and payers_signed_status.updated_date >='${last_updated_date}'`
            console.log("if updated_date", last_updated_date)

        }
        console.log("last_updated_date", last_updated_date)
        console.log(sql, "sql")
        let result = await pool.query(sql)

        if (result.rows.length == '') {
            console.log("No Content (No Payer found for the given ID)")
            res.send({
                status_code: "204", message: "No Payer found for the given Payer ID / Member ID"
            })

        }
        else {
            var payerdata = result.rows;
            var finaldata = [];
            console.log("payerdata length", payerdata.length)
            const AllPayerDetails = async () => {
                const endpointList = payerdata.map(x => getAllPayerEndPoints(x.payer_id));
                const results = await Promise.all(endpointList);
                console.log(results, "results")
                res.status(200).json({
                    status_code: "200", message: "List of Payers Successfully Fetched", results
                })

            };

            AllPayerDetails()
        }

    }
    catch (err) {
        console.log(err)
    }





}

const getAllpayers = async (req, res) => {
    const { email, password, last_updated_date } = req.body
    // var sql = `select * from users where email ='${email}'`
    var sql = `SELECT  users.*,payers_organisation_detail.email as orgEmail,payers_organisation_detail.payer_id as payer_id,
    payers_organisation_detail.is_approve FROM users
    left join payers_organisation_detail on payers_organisation_detail.user_id =users.user_id
    where users.email = '${email}'`
    console.log('sql=', sql)
    let recordset = await pool.query(sql)

    var results = recordset.rows
    if (results.length == 0) {
        console.log("User does not exist")
        res.status(404).send({ "code": 404, message: "User does not exist" })
    }
    else {

        console.log(password, " bcrypt password ", results[0].password)
        let compare = bcrypt.compareSync(password, results[0].password)
        var user_id = results[0].user_id
        var payer_id = results[0].payer_id
        console.log(payer_id, user_id)
        if (compare == false) {
            console.log("Email and password does not match")
            res.status(400).send({ "code": 400, message: "Email and password does not match" })
        }

        else {
            var sql = `SELECT payers_organisation_detail.payer_id,name as payer_name,
                        organisation_display_name as payer_alias,organisation_type as payer_type,
                     current_timestamp AT TIME ZONE 'Asia/Kolkata' as last_updated_date
                             FROM  payers_signed_status
                             LEFT JOIN payers_organisation_detail  ON payers_signed_status.destinatoin_payer_id = payers_organisation_detail.payer_id
                                left join organisation_type on organisation_type.id =payers_organisation_detail.organisation_type_id
                                  left join users on users.user_id =payers_organisation_detail.user_id
                               where payers_signed_status.payer_status ='Signed' and  payers_signed_status.source_payer_id =${payer_id}`
            console.log("payers_organisation_detail", sql)

            if (last_updated_date != undefined) {
                sql = sql + ` and payers_signed_status.updated_date >='${last_updated_date}'`
                console.log("if updated_date", last_updated_date)

            }


            console.log("last_updated_date", last_updated_date)

            let result = await pool.query(sql)

            if (result.rows.length == '') {
                console.log("No Content (No Payer found for the given ID)")
                res.send({
                    status_code: "204", message: "No Payer found for the given Payer ID / Member ID"
                })

            }
            else {
                var payerdata = result.rows;

                console.log("payerdata", payerdata)

                console.log("payerdata length", payerdata.length)
                const AllPayerDetails = async () => {
                    const endpointList = payerdata.map(x => getAllPayerEndPoints(x.payer_id));
                    const results = await Promise.all(endpointList);
                    console.log(results, "results")
                    res.status(200).json({
                        status_code: "200", message: "List of Payers Successfully Fetched", results
                    })

                };

                AllPayerDetails()


            }

        }
    }
}
const getToken = async (req, res) => {
    const { email, password } = req.body

    var sql = `select email,password from users where email= '${email}'`
    console.log('sql=', sql)
    let recordset = await pool.query(sql)
    var results = recordset.rows
    if (results.length == 0) {
        console.log("User does not exist")
        res.status(404).send({ "code": 404, message: "User does not exist" })
    }
    else {

        console.log(password, " bcrypt password ", results[0].password)
        let compare = bcrypt.compareSync(password, results[0].password)

        var user_id = results[0].user_id
        console.log(user_id)
        const token = jwt.sign(
            {
                email: results[0].email,
                user_id: results[0].user_id
            },
            ACCESS_TOKEN_SECRET,
            {
                expiresIn: "2h",
            }
        );

        // save user token
        results[0].token = token;

        if (compare == false) {
            console.log("Email and password does not match")
            res.status(400).send({ "code": 400, message: "Email and password does not match" })
        }
        else {

            res.send({
                status_code: "200",
                token, message: 'successfully authenticated'
            })



        }

    }
}
const getAdapterendpoint = async (req, res, err) => {
    var { payer_id, inserted_date, payer_name, updated_date } = req.query
    pool.connect((error) => {
        if (error) {
            res.send({
                status_code: "500",
                message: 'Internal server Error (database connection error).'
            })
        }
        else {


            pool.query("select payer_id from payers_organisation_detail where Lower(name)=lower($1) or payer_id=$2", [payer_name, payer_id], (error, results) => {

                if (error) {
                    res.send({
                        "code": 400,
                        message: 'Request parameter undefined or not passed successfully'
                    })
                }
                else {

                    if (results.rows.length == '') {
                        console.log("No Content (No Payer found for the given ID)")
                        res.send({
                            status_code: "204", message: "No Payer found for the given Payer ID / Member ID"
                        })

                    }
                    else {
                        //  var payer_name = results.rows[0].name
                        var payer_id = results.rows[0].payer_id

                        var sql = `SELECT payers_organisation_detail.payer_id,name as payer_name,
                        organisation_display_name as payer_alias,organisation_type as payer_type,
                     current_timestamp AT TIME ZONE 'Asia/Kolkata' as last_updated_date
                             FROM  payers_signed_status
                             LEFT JOIN payers_organisation_detail  ON payers_signed_status.destinatoin_payer_id = payers_organisation_detail.payer_id
                                left join organisation_type on organisation_type.id =payers_organisation_detail.organisation_type_id
                                  left join users on users.user_id =payers_organisation_detail.user_id
                               where payers_signed_status.payer_status ='Signed' and  payers_signed_status.source_payer_id =${payer_id}`

                        if (updated_date != undefined) {
                            sql = sql + `and payers_signed_status.updated_date >='${updated_date}'`
                            console.log("if updated_date", updated_date)

                        }


                        console.log("updated_date", updated_date)
                        console.log(sql, "sql")
                        pool.query(sql, (error, result) => {
                            if (error) {
                                res.send({
                                    "code": 400,
                                    message: 'Request parameter undefined or not passed successfully'
                                })
                            }
                            else {
                                if (result.rows.length == '') {
                                    console.log("No Content (No Payer found for the given ID)")
                                    res.send({
                                        status_code: "204", message: "No Payer found for the given Payer ID / Member ID"
                                    })

                                }
                                else {
                                    var payerdata = result.rows;
                                    console.log("payerdata length", payerdata.length)

                                    const AllPayerDetails = async () => {
                                        const endpointList = payerdata.map(x => getAllPayerEndPoints(x.payer_id));
                                        const results = await Promise.all(endpointList);
                                        console.log(results, "results")
                                        res.status(200).json({
                                            status_code: "200", message: "List of Payers Successfully Fetched", results
                                        })

                                    };

                                    AllPayerDetails()

                                }
                            }
                        })
                    }


                }


            })
        }

    })
}
const getAllPayerEndPoints = async (respayerid) => {
    var sandendpointarr = [];
    var prodendpointarr = [];
    var allEndpointarr = []
    var bothendpointarr = []
    var resdata = {};
    var prodresdata
    var sandresdata
    var bothresdata


    var sql2 = `SELECT payer_end_point_mapping.id,payer_end_points.id as payer_endpoint_id,end_point_master.endpoint_name,organisation_type,current_timestamp AT TIME ZONE 'Asia/Kolkata' as last_updated_date,authorize_url,auth_type,token_url,
    payer_end_points.base_url,payer_end_points.other_endpoint_name, payer_end_points.updated_date, payer_end_points.auth_scope, payer_end_points.payer_environment_type,
    payers_organisation_detail.name,payers_organisation_detail.payer_id
     FROM payer_end_points
    LEFT JOIN payers_organisation_detail ON payer_end_points.payer_id = payers_organisation_detail.payer_id
    LEFT join organisation_type on organisation_type.id =payers_organisation_detail.organisation_type_id
   LEFT JOIN payer_end_point_mapping ON payer_end_point_mapping.payer_endpoint_id = payer_end_points.id
   LEFT JOIN end_point_master ON end_point_master.id = payer_end_point_mapping.endpoint_master_id

   LEFT JOIN users ON users.user_id = payers_organisation_detail.user_id
   where  payers_organisation_detail.payer_id=${respayerid}`
    // console.log("payerdata sql2", sql2)
    //    left join payers_environment on payers_environment.id =payers_organisation_detail.payer_environment
    //payers_organisation_detail.payer_environment as payer_environment_id,payers_environment.payer_environment
    let results = await pool.query(sql2)

    var endpointdata1 = results.rows
    var endpointdata = endpointdata1.filter(function (e) { return e.endpoint_name != '' && e.endpoint_name != null });
    var ArrayPayerEnv = ["Both", "Production", "Sandbox"]
    console.log("endpointdata length", endpointdata.length)


    if (endpointdata.length > 0) {
        console.log(endpointdata, "endpointdata")
        for (var i = 0; i < endpointdata.length; i++) {

            console.log(endpointdata[i].payer_environment_type, "payer_environment_type")


            if (endpointdata[i].payer_environment_type == 'Production') {
                prodendpointarr.push(endpointdata[i].endpoint_name);
                prodresdata = {
                    "base_url": endpointdata[i].base_url,
                    "auth_scope": endpointdata[i].auth_scope,
                    "payer_type": endpointdata[i].organisation_type,
                    "payer_environment_type": endpointdata[i].payer_environment_type,
                    "name": endpointdata[i].name,
                    "payer_id": endpointdata[i].payer_id,
                    "authorize_url": endpointdata[i].authorize_url,
                    "auth_type": endpointdata[i].auth_type,
                    "token_url": endpointdata[i].token_url,
                    "endpoints": prodendpointarr,
                    "last_updated_date_time": endpointdata[i].last_updated_date,
                }
            }
            else if (endpointdata[i].payer_environment_type == 'Sandbox') {
                sandendpointarr.push(endpointdata[i].endpoint_name);
                sandresdata = {
                    "base_url": endpointdata[i].base_url,
                    "auth_scope": endpointdata[i].auth_scope,
                    "payer_type": endpointdata[i].organisation_type,
                    "payer_environment_type": endpointdata[i].payer_environment_type,
                    "name": endpointdata[i].name,
                    "payer_id": endpointdata[i].payer_id,
                    "authorize_url": endpointdata[i].authorize_url,
                    "auth_type": endpointdata[i].auth_type,
                    "token_url": endpointdata[i].token_url,
                    "endpoints": sandendpointarr,
                    "last_updated_date_time": endpointdata[i].last_updated_date,
                }
            }
            else if (endpointdata[i].payer_environment_type == 'Both (Production And Sandbox)') {
                bothendpointarr.push(endpointdata[i].endpoint_name);
                bothresdata = {
                    //sandresdata,prodresdata
                    "base_url": endpointdata[i].base_url,
                    "auth_scope": endpointdata[i].auth_scope,
                    "payer_type": endpointdata[i].organisation_type,
                    "payer_environment_type": endpointdata[i].payer_environment_type,
                    "name": endpointdata[i].name,
                    "payer_id": endpointdata[i].payer_id,
                    "authorize_url": endpointdata[i].authorize_url,
                    "auth_type": endpointdata[i].auth_type,
                    "token_url": endpointdata[i].token_url,
                    "endpoints": bothendpointarr,
                    "last_updated_date_time": endpointdata[i].last_updated_date,
                }
            }

        }

        allEndpointarr.push(sandresdata)
        allEndpointarr.push(prodresdata)
        allEndpointarr.push(bothresdata)
        return allEndpointarr;

    }




}

module.exports = {
    getMypayers,
    getToken,
    getAllpayers,
    getAdapterendpoint

}
















