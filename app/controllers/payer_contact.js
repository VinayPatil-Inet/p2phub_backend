var pool = require('../config/db.config')

const getPayerContact = (request, response) => {
    pool.query('SELECT * FROM payer_contact', (error, results) => {
        if (error) {
            throw error
        }
        var data = results.rows
        response.status(200).json({ body: 'Success', data })
        console.log(data)

    })
}



const createPayerContact = (request, response) => {
    const { name, email, subject, message, inserted_date, inserted_by } = request.body
    pool.query('INSERT INTO payer_contact ( name, email, subject, message, inserted_date, inserted_by) VALUES ($1, $2, $3, $4, $5, $6)', [ name, email, subject, message, inserted_date, inserted_by], (error, results) => {
        if (error) {
            throw error
        }
        response.status(200).json({ body: 'successfully created a payer contact' })
        // console.log(data)

    })
}
const deletePayerContact = (request, response) => {
    const id = parseInt(request.params.id)
    pool.query('DELETE FROM payer_contact WHERE id = $1', [id], (error, results) => {
        if (error) {
            throw error
        }
       // response.status(200).send(`User deleted with ID: ${id}`)
        response.status(200).json({ body: 'successfully deleted a payer contact' })
       
    })
}
const getIdPayerContact = (request, response) => {
    const id = parseInt(request.params.id)
    pool.query('select  * FROM payer_contact WHERE id = $1', [id], (error, results) => {
        if (error) {
            throw error
        }
       // response.status(200).send(`User deleted with ID: ${id}`)
        response.status(200).json({ body: 'successfully deleted a payer contact' })
       
    })
}

const updatePayerContact = (request, response) => {
    const id = parseInt(request.params.id)
    const { name, email, subject, message } = request.body
    var sql =`update  payer_contact SET name = '${name}', email = '${email}',  subject = '${subject}', 
     message ='${message}'  WHERE id = ${id}`
     console.log(sql,"sql")
    pool.query(sql,
    //   "UPDATE payer_contact SET name = $1, email = $2,  subject = $3,  message =$4  WHERE id = $5",
    //   [name, email, subject, message, id],
      (error, recoredset) => {
        if (error) {
        throw error
        }
        response.status(200).json({ body: 'successfully update a payer contact' })
      
      })
}





module.exports = {
    getPayerContact,
    createPayerContact,
    updatePayerContact,
    deletePayerContact,
    getIdPayerContact
}










