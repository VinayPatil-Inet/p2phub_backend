var pool = require('../config/db.config')

const getPayersSignedStatus = (request, response) => {
    var sql = `SELECT * FROM Payers_Signed_Status`
    pool.query(sql, (error, results) => {
        if (error) {
            throw error
        }
        var data = results.rows
        response.status(200).json({ body: 'Success', data })
        console.log(data)

    })
}
const getAllNotifactions = (request, response) => {
    const payer_id = parseInt(request.params.payer_id)
    var sql = `SELECT * FROM notifications where payer_id =${payer_id} order by id desc`
    pool.query(sql, (error, results) => {
        if (error) {
            throw error
        }
        var data = results.rows
        response.status(200).json({ body: 'Success', data })
        console.log(data)

    })
}


const createPayersSignedStatus = (req, res) => {
    const { source_payer_id, destinatoin_payer_id, user_id } = req.body
    var sql = `select * from payers_signed_status where source_payer_id ='${source_payer_id}'and destinatoin_payer_id =
    '${destinatoin_payer_id}'`
    console.log('sql=', sql)
    pool.query(sql, function (error, recordset) {
        if (error) {
            res.send({
                "code": 400,
                message: 'there are some error with query'
            })
        }
        else {
            var data = recordset.rows
            if (data[0]) {
                console.log("source_payer_id already exits")
                res.status(200).json({ body: 'data already exits ', data })
                console.log(data, "payers_signed")
                // res.status(404).send({ "code": 404, message: "source_payer_id already exits" })
            }
            else {
                //  console.log(results, "user")
                pool.connect(function () {

                    var sqlQuery = `INSERT INTO payers_signed_status(
                                     source_payer_id, destinatoin_payer_id, inserted_by)
                                     VALUES ('${source_payer_id}','${destinatoin_payer_id}','${user_id}')  RETURNING *;`
                    console.log(sqlQuery, "payers_signed_status")
                    pool.query(sqlQuery, (error, results) => {
                        if (error) {
                            throw error
                        }
                        var data = results.rows

                        res.status(200).json({ body: 'successfully created a payers signed status ', data })
                    });

                    // })
                })
            }

        }

    })

}
const RegReqPayersSignedStatus = (request, response) => {
    //const id = parseInt(request.params.id)
    const payer_id = parseInt(request.params.payer_id)
    var sql = `SELECT payers_organisation_detail.payer_id,name, payers_organisation_detail.email,payers_organisation_detail.user_id,payer_signed_status_id, source_payer_id,
    destinatoin_payer_id, contract_file_path, payer_status,payers_signed_status.inserted_by,organisation_type as type,
	payers_signed_status.updated_date,users.first_name AS fullname
        FROM  payers_signed_status
        LEFT JOIN payers_organisation_detail  ON payers_signed_status.source_payer_id = payers_organisation_detail.payer_id
		   left join organisation_type on organisation_type.id =payers_organisation_detail.organisation_type_id
		     left join users on users.user_id =payers_organisation_detail.user_id
          where
            payers_signed_status.payer_status ='Initiated' and   payers_signed_status.destinatoin_payer_id =${payer_id} order by payer_id desc`


    console.log(sql, "sql")
    pool.query(sql, (error, recoredset) => {
        if (error) {
            throw error
        }
        var data = recoredset.rows
        response.status(200).json({ body: 'successfully', data })

    })
}

const RegReqPayersSignedStatusCount = (request, response) => {
    //const id = parseInt(request.params.id)
    const payer_id = parseInt(request.params.payer_id)
    var sql = `select count(*) count_name from public.payers_signed_status where payer_status ='Initiated'
     and destinatoin_payer_id =${payer_id}`
    console.log(sql, "sql")
    pool.query(sql, (error, recoredset) => {
        if (error) {
            throw error
        }
        var data = recoredset.rows
        console.log(data, "RegReqPayersSignedStatusCount")
        response.status(200).json({ body: 'successfully', data })

    })
}
const RegPayersSignedStatusCount = (request, response) => {
    //const id = parseInt(request.params.id)
    const payer_id = parseInt(request.params.payer_id)
    var sql = `select count(*)  count_name from public.payers_signed_status where payer_status ='Signed'
    and source_payer_id =${payer_id} `

    console.log(sql, "sql")
    pool.query(sql, (error, recoredset) => {
        if (error) {
            throw error
        }
        var data = recoredset.rows
        console.log(data, "RegPayersSignedStatusCount")
        response.status(200).json({ body: 'successfully', data })

    })
}

const RegReqPayersPendingStatus = (request, response) => {
    //const id = parseInt(request.params.id)
    const payer_id = parseInt(request.params.payer_id)
    var sql = `SELECT payers_organisation_detail.payer_id,name, payers_organisation_detail.email,payers_organisation_detail.user_id,payer_signed_status_id, source_payer_id,
    destinatoin_payer_id, contract_file_path, payer_status,payers_signed_status.inserted_by,organisation_type as type,
	payers_signed_status.updated_date,users.first_name  AS fullname
        FROM  payers_signed_status
        LEFT JOIN payers_organisation_detail  ON payers_signed_status.destinatoin_payer_id = payers_organisation_detail.payer_id
		   left join organisation_type on organisation_type.id =payers_organisation_detail.organisation_type_id
		     left join users on users.user_id =payers_organisation_detail.user_id
          where
            payers_signed_status.payer_status ='Initiated' and   payers_signed_status.source_payer_id =${payer_id}  order by payer_id`

    console.log(sql, "sql")
    pool.query(sql, (error, recoredset) => {
        if (error) {
            throw error
        }
        var data = recoredset.rows
        console.log(data, "RegReqPayersPendingStatus")
        response.status(200).json({ body: 'successfully', data })

    })
}

const RegReqPayersRejectStatus = (request, response) => {
    //const id = parseInt(request.params.id)
    const payer_id = parseInt(request.params.payer_id)
    var sql = `SELECT payers_organisation_detail.payer_id,name, payers_organisation_detail.email,payers_organisation_detail.user_id,payer_signed_status_id, source_payer_id,
    destinatoin_payer_id, contract_file_path, payer_status,payers_signed_status.inserted_by,organisation_type as type,
	payers_signed_status.updated_date,users.first_name  AS fullname
        FROM  payers_signed_status
        LEFT JOIN payers_organisation_detail  ON payers_signed_status.destinatoin_payer_id = payers_organisation_detail.payer_id
		   left join organisation_type on organisation_type.id =payers_organisation_detail.organisation_type_id
		     left join users on users.user_id =payers_organisation_detail.user_id
          where
            payers_signed_status.payer_status ='Rejected' and   payers_signed_status.source_payer_id =${payer_id}`

    console.log(sql, "sql")
    pool.query(sql, (error, recoredset) => {
        if (error) {
            throw error
        }
        var data = recoredset.rows
        response.status(200).json({ body: 'successfully', data })

    })
}



const updatePayersSignedStatusAccept = (request, response) => {
     const payer_signed_status_id = parseInt(request.params.payer_signed_status_id)

        console.log("statusid==", payer_signed_status_id);

        var sql = `SELECT source_payer_id ,destinatoin_payer_id FROM payers_signed_status WHERE payer_signed_status_id = '${payer_signed_status_id}'`
        console.log(sql, "getBothPayers")
        pool.query(sql, (error, result) => {
            if (error) {
                throw error
            }
            console.log("payers_signed_status payer details== ", result.rows);
            var sourceid = result.rows[0].source_payer_id;
            var destid = result.rows[0].destinatoin_payer_id;
            console.log("sourceid----- ", sourceid, destid)
            sqlQuery = `select a.payer_id as spayerid, a.name sorganization_name, a.email semail,c.user_id suser_id,d.user_id duser_id ,c.first_name sfirst_name,b.payer_id dpayerid, b.name dorganization_name,
            b.email demail, d.first_name dfirst_name from payers_organisation_detail
            inner join payers_organisation_detail a on a.payer_id = ${sourceid}
            inner join payers_organisation_detail b on b.payer_id = ${destid}
            inner join users c on c.user_id =a.user_id
            inner join users d on d.user_id = b.user_id limit 1`
            console.log(sqlQuery, "sqlQuery")
            pool.query(sqlQuery, (error, results) => {

                if (error) {
                    throw error
                }
                var data = results.rows
             //   response.status(200).json({ body: 'Success', data })
                console.log("both payer details== ", data)
                var destinatoin_payer_name = results.rows[0].dfirst_name
                var source_payer_name = results.rows[0].sfirst_name
                var user_id = results.rows[0].duser_id
                console.log("destinatoin_payer_name== ", destinatoin_payer_name)
                console.log("source_payer_name== ", source_payer_name)


                var notificationtext = 'Registration request has been accepted by '
                var button_text = 'See Registered Orgnizations'

                var sqlQuery3 = `INSERT INTO notifications (user_id,payer_id,name,
                redirect_url,notificationtext,button_text)  values ('${user_id}','${sourceid}','${destinatoin_payer_name}','${'./RegisteredOrgnization'}',
                '${notificationtext}','${button_text}')`

                console.log(sqlQuery3, "notifications")
                pool.query(sqlQuery3, (error, recoredset) => {
                    if (error) {
                        throw error
                    }
                    //   response.status(200).json({ body: 'successfully update a payer contact' })

                    var sqlQuery2 = `UPDATE payers_signed_status SET payer_status = 'Signed'
                    WHERE payer_signed_status_id = ${payer_signed_status_id}`
                    console.log(sqlQuery2, "sqlQuery2")
                    pool.query(sqlQuery2, (error, recoredset) => {
                        if (error) {
                            throw error
                        }
                        response.status(200).json({ body: 'successfully update a payer contact' })


                    })

                })



            })

        })
}
const updatePayersSignedStatusReject = (request, response) => {
    const payer_signed_status_id = parseInt(request.params.payer_signed_status_id)
    console.log("statusid==", payer_signed_status_id);

    var sql = `SELECT source_payer_id ,destinatoin_payer_id FROM payers_signed_status WHERE payer_signed_status_id = '${payer_signed_status_id}'`
    console.log(sql, "getBothPayers")
    pool.query(sql, (error, result) => {
        if (error) {
            throw error
        }
        console.log("payers_signed_status payer details== ", result.rows);
        var sourceid = result.rows[0].source_payer_id;
        var destid = result.rows[0].destinatoin_payer_id;
        console.log("sourceid----- ", sourceid, destid)
        sqlQuery = `select a.payer_id as spayerid, a.name sorganization_name, a.email semail, c.first_name sfirst_name,b.payer_id dpayerid, b.name dorganization_name,
        b.email demail, d.first_name dfirst_name from payers_organisation_detail
        inner join payers_organisation_detail a on a.payer_id = ${sourceid}
        inner join payers_organisation_detail b on b.payer_id = ${destid}
        inner join users c on c.user_id =a.user_id
        inner join users d on d.user_id = b.user_id limit 1`
        console.log(sqlQuery, "sqlQuery")

        pool.query(sqlQuery, (error, results) => {

            if (error) {
                throw error
            }
            var data = results.rows
          //  response.status(200).json({ body: 'Success', data })
            console.log("both payer details== ", data)
            var destinatoin_payer_name = results.rows[0].dfirst_name
            var source_payer_name = results.rows[0].sfirst_name
            console.log("destinatoin_payer_name== ", destinatoin_payer_name)
            console.log("source_payer_name== ", source_payer_name)


            var notificationtext = 'Registration request has been rejected by'
            var button_text = 'See Rejected Requests '


            var sqlQuery3 = `INSERT INTO notifications (payer_id,name,
            redirect_url,notificationtext,button_text)  values ('${sourceid}','${destinatoin_payer_name}','${'./RegisteredOrgnization?tab=fourth'}',
            '${notificationtext}','${button_text}')`

            console.log(sqlQuery3, "notifications")

            pool.query(sqlQuery3, (error, recoredset) => {
                if (error) {
                    throw error
                }
                //   response.status(200).json({ body: 'successfully update a payer contact' })



                var sqlQuery2 = `UPDATE payers_signed_status SET payer_status = 'Rejected' WHERE payer_signed_status_id = ${payer_signed_status_id}`
                console.log(sqlQuery2, "sqlQuery2")
                pool.query(sqlQuery2, (error, recoredset) => {
                    if (error) {
                        throw error
                    }
                 //   response.status(200).json({ body: 'successfully update a payer contact' })


                })



                    })


        })

    })
}
const updatePayersSignedStatus = (request, response) => {
    const { payer_status, id } = request.body

    console.log("statusid==", id);

    var sql = `SELECT source_payer_id ,destinatoin_payer_id FROM payers_signed_status WHERE payer_signed_status_id = ${id}`
    console.log(sql, "getBothPayers")
    pool.query(sql, (error, result) => {
        if (error) {
            throw error
        }
        console.log("payers_signed_status payer details== ", result.rows);
        var sourceid = result.rows[0].source_payer_id;
        var destid = result.rows[0].destinatoin_payer_id;
        console.log("sourceid----- ", sourceid, destid)
        sqlQuery = `select a.payer_id as spayerid, a.name sorganization_name, a.email semail,a.address1,a.address1,a.city, e.state_name, a.country,f.organisation_type, a.zip_code,a.website,a.ein,a.policylink,a.conditionlink, c.first_name sfirst_name,b.payer_id dpayerid, b.name dorganization_name,
        b.email demail, d.first_name dfirst_name from payers_organisation_detail
        inner join payers_organisation_detail a on a.payer_id = ${sourceid}
        inner join payers_organisation_detail b on b.payer_id = ${destid}
        inner join users c on c.user_id =a.user_id
        inner join users d on d.user_id = b.user_id
        inner join location e on e.id =a.state
		inner join organisation_type f on f.id =a.organisation_type_id limit 1`
        console.log(sqlQuery, "sqlQuery")
        pool.query(sqlQuery, (error, results) => {

            if (error) {
                throw error
            }
            var data = results.rows
         //   response.status(200).json({ body: 'Success', data })
            console.log("both payer details== ", data)
            var destinatoin_payer_name = results.rows[0].dfirst_name
            var source_payer_name = results.rows[0].sfirst_name
            console.log("destinatoin_payer_name== ", destinatoin_payer_name)
            console.log("source_payer_name== ", source_payer_name)


            var notificationtext = 'sent you a registration request on'
            var button_text = 'See Request'

            var sqlQuery3 = `INSERT INTO notifications (payer_id,name,
            redirect_url,notificationtext,button_text)  values ('${destid}','${source_payer_name}',
           '${'./RegisteredOrgnization?tab=second'}',
            '${notificationtext}','${button_text}')`
            pool.query(sqlQuery3, (error, recoredset) => {
                if (error) {
                    throw error
                }


            });

            var notificationtext = 'Registration request has been sent to'
            var button_text = 'See Pending Request'

            var sqlQuery3 = `INSERT INTO notifications (payer_id,name,
            redirect_url,notificationtext,button_text)  values ('${sourceid}','${destinatoin_payer_name}',
           '${'./RegisteredOrgnization?tab=third'}','${notificationtext}','${button_text}')`


            console.log(sqlQuery3, "notifications")
            pool.query(sqlQuery3, (error, recoredset) => {
                if (error) {
                    throw error
                }
                //   response.status(200).json({ body: 'successfully update a payer contact' })

                var sqlQuery2 = `UPDATE payers_signed_status SET payer_status = '${payer_status}' WHERE payer_signed_status_id = ${id}`
                console.log(sqlQuery2, "sqlQuery2")
                pool.query(sqlQuery2, (error, finalresult) => {
                    if (error) {
                        throw error
                    }

                })
              //  var data = finalresult.rows
              var sqlQuery4 =`SELECT payers_organisation_detail.payer_id,name, payers_organisation_detail.email,payers_organisation_detail.user_id,
              policylink,conditionlink,website,users.username,logo,payers_organisation_detail.address1,payers_organisation_detail.city,
			location.state_name as state,
              organisation_display_name,primary_developer_name,primary_developer_title,primary_developer_email,
              app_url_pre_production,api_url_production,
              payer_signed_status_id, source_payer_id,
              destinatoin_payer_id, payer_status,payers_signed_status.inserted_by,organisation_type as type,
              payers_signed_status.updated_date,users.first_name  AS fullname
                  FROM  payers_signed_status
                  LEFT JOIN payers_organisation_detail  ON payers_signed_status.destinatoin_payer_id = payers_organisation_detail.payer_id
                     left join organisation_type on organisation_type.id =payers_organisation_detail.organisation_type_id
					   left join location on location.id =payers_organisation_detail.state
                       left join users on users.user_id =payers_organisation_detail.user_id
                    where payers_signed_status.source_payer_id =${sourceid}`

                   console.log(sqlQuery4, "sqlQuery4")
                   pool.query(sqlQuery4, (error, final) => {
                       if (error) {
                           throw error
                       }
                       var data = final.rows
                       console.log(data,"Source payer details")
                       response.status(200).json({ body: 'successfully update a payer contact' ,data})


                   })

             //   response.status(200).json({ body: 'successfully update a payer contact',data })


            })



        })

    })



}


module.exports = {

    createPayersSignedStatus,
    RegPayersSignedStatusCount,
    RegReqPayersSignedStatusCount,
    getPayersSignedStatus,
    updatePayersSignedStatus,
    updatePayersSignedStatusReject,
    RegReqPayersRejectStatus,
    RegReqPayersPendingStatus,
    RegReqPayersSignedStatus,
    updatePayersSignedStatusAccept,
    getAllNotifactions
}










