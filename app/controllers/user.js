var pool = require('../config/db.config')
var bcrypt = require("bcryptjs");
var crypto = require("crypto");
const Cryptr = require('cryptr');
var jwtTokens = require("../controllers/jwt-helpers")
var randtoken = require('rand-token');
var nodemailer = require('nodemailer');
const requestad = require("request");
const jwt = require("jsonwebtoken");
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey('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');

var ACCESS_TOKEN_SECRET = 'padma'

const getUsers = (req, res) => {

    var sqlQuery1 = `SELECT  first_name||' '||last_name as name,
     phone, email, inserted_date, is_active, password ,user_id FROM Users order by user_id DESC`
    //  console.log('sqlquery=', sqlQuery)
    pool.query(sqlQuery1, function (error, results) {
        if (error) {
            res.send({
                "code": 400,
                message: 'there are some error with query'
            })
        } else {
            var data = results.rows
            res.status(200).json({ body: 'Success', data })


        }

    })
}

const getIdByUser = (req, res) => {
    //
    const id = parseInt(req.params.id)

    var sqlQuery = `SELECT * from users where user_id  = ${id}`

    console.log('sqlquery=', sqlQuery)
    pool.query(sqlQuery, function (error, results) {
        if (error) {
            res.send({
                "code": 400,
                message: 'there are some error with query'
            })
        } else {
            var data = results.rows
            res.status(200).json({ body: 'Success', data })
            //  console.log(data)

        }

    })
}

const getEINByOrganization = (req, res) => {
   // const ein = req.body.ein
    // const ein = req.params.ein;

    var sqlQuery3 = `SELECT  ein,name as organisation_name, email FROM payers_organisation_detail `
   // var sqlQuery3 = `SELECT ein,email,user_id,payer_id FROM payers_organisation_detail where ein='${ein}' `
    console.log(sqlQuery3, "sqlQuery3")
    pool.query(sqlQuery3, function(error, results){
        if (error) {
            res.send({
                "code": 400,
                message: 'there are some error with query'
            })
        } 
            var data = results.rows
            res.status(200).json({ body: 'Success', data })
    })
 
}
      
   

const getSearchbyUser = (req, res) => {
    var { last_name, first_name, email } = req.body

    pool.query("select * from users where last_name=$1 or first_name= $2 or email =$3"[last_name, first_name, email], (error, results) => {
        if (error) {
            throw error
        }

        var data = results.rows
        res.status(200).json({ body: 'Success', data })
        console.log(data)
    })
}

const verifyEmail = (req, res) => {
    token = req.query.email;
    console.log(token)
    if (token) {
        try {
            jwt.verify(token, JWT_ACC_ACTIVATE, (e, decoded) => {
                if (e) {
                    console.log(e)
                    res.send({ code: 403, message: "Expired link. Signup again" })
                    console.log("Expired link. Signup again", e);
                } else {
                    var email = decoded.email;
                    pool.connect(function () {
                        var query = `UPDATE  users  SET  is_active = 'true'  WHERE  email = '${email}' RETURNING *`
                        pool.query(query, function (err, results, _fields) {
                            //  var data = results.rows
                            console.log(query, "query")
                            if (err) {
                                console.log(err);
                                console.log("Please try again!");
                                res.send("Please try again!");
                            } else {
                                console.log("updated Successfully");
                                console.log(data, "Successfully");
                                var data = results.rows
                                res.redirect('https://payerhub.health-chain.io/userlogin?success=' + encodeURIComponent('You have successfuly verified email'));
                                // res.redirect(process.env.REACT_APP_BASE_FRONTEND_URL + '/userlogin?success=' + encodeURIComponent('You have successfuly verified email'));

                            }

                        });
                    });
                }
            });
        } catch (err) {

            res.send({ code: 403, message: "Expired link. Signup again" })
            console.log("Expired link. Signup again", err);
        }
    } else {
        res.send({ code: 403, message: "Expired link. Signup again" })
    }




}
const updateUser = (request, response) => {
    const user_id = parseInt(request.params.user_id)
    const { userphone, useremail, firstname, lastname, user_types_id } = request.body

    var sqlquery = `UPDATE Users SET phone = ${userphone}, email = '${useremail}',first_name ='${firstname}',last_name ='${lastname}'
                WHERE user_id = ${user_id} RETURNING * `

    console.log(sqlquery, "sqlquery")
    pool.query(sqlquery, (error, results) => {
        if (error) {
            console.log(error)
            throw error
        }
        var data = results.rows[0]
        response.status(200).json({ body: 'successfully update a user', data })
    })


    // var sql = `select * from users where email ='${useremail}' `
    // console.log('sql=', sql)
    // pool.query(sql, function (error, recordset) {
    //     if (error) {
    //         res.send({
    //             "code": 400,
    //             message: 'there are some error with query'
    //         })
    //     }
    //     else {
    //         var results = recordset.rows
    //         if (results[0]) {
    //             console.log("Email already exits")
    //             response.status(404).send({ "code": 404, message: "Email already exits" })
    //         }
    //         else {

    //  }
    // }
    // })



}



const deleteUser = (request, response) => {
    const id = parseInt(request.params.id)
    pool.query('DELETE FROM users WHERE user_id = $1', [id], (error, results) => {
        if (error) {
            throw error
        }
        response.status(200).json({ body: 'successfully deleted a user' })

    })
}
const ChangePassword = async (req, res) => {
    const user_id = parseInt(req.params.user_id)
    try {
        var params = req.body;
        var Query = `select * from users  WHERE user_id = ${user_id}`
        pool.query(Query, function (err, result) {
            if (err) {
                console.log("Error:", err)
                var obj = {
                    Status: 400,
                    message: err.message
                }
                res.json(obj)

            }
            else {
                var userData = {};

                if (result.rows && result.rows.length != 0) {
                    userData = result.rows[0];
                    userId = result.rows[0].user_id
                    console.log(userId, "userId")
                    var object_id = result.rows[0].object_id

                    console.log(userData, "userData")
                    var hashedPassword = bcrypt.hashSync(params.password, 8);
                    var Query2 = `UPDATE users SET password = '${hashedPassword}'  WHERE user_id = ${user_id}`;
                    console.log("Query2", Query2);

                    pool.query(Query2, function (err, result) {
                        if (err) {
                            var obj = {
                                Status: 400,
                                message: err.message
                            }
                            res.json(obj)
                        }
                        else {
                            getToken(object_id, params.password);
                            var obj = {
                                Status: 200,
                                message: "Password changed successfully",

                            }
                            res.json(obj)
                        }
                    })
                }
                else {
                    var obj = {
                        Status: 400,
                        message: "User did not matched"
                    }
                    res.json(obj)
                }


            }
        })

    }
    catch (err) {
        var obj = {
            Status: 400,
            message: err.message
        }
        res.json(obj)
    }

}
function UpdateADUser(accessToken, objectid, password) {
    console.log("UpdateADUser called =" + accessToken);
    const userdetails = {
        "department": "Support",
        "passwordProfile": {
            "password": password,
            "forceChangePasswordNextSignIn": false
        }
    };
    requestad.patch({
        url: "https://graph.microsoft.com/v1.0/users/" + objectid,
        body: JSON.stringify(userdetails),
        headers: {
            "Authorization": "Bearer " + accessToken,
            "Content-Type": "application/json"
        }
    }, function (err, response, body) {
        console.log("UpdateADUser body======== ", body);
        console.log("UpdateADUser err======== ", err);
        console.log("UpdateADUser response======== ", response);
    });
}
function getToken(objectid, password) {
    console.log("getToken called objectid =" + objectid);
    const endpoint = "https://login.microsoftonline.com/0bd024f1-e61c-461f-b68c-96b715e96cee/oauth2/v2.0/token";
    const requestParams = {
        grant_type: "client_credentials",
        client_id: "bc0d1797-d689-4cfb-a5cc-b94c8e42fd57",
        client_secret: "2-C7Q~BvYmzJeFuxDUdDLfj7gd-BRkL7tyzB~", //client secret value
        scope: "https://graph.microsoft.com/.default"
    };

    requestad.post({ url: endpoint, form: requestParams }, function (err, response, body) {
        if (err) {
            console.log("error");
        }
        else {
            console.log("getToken Body=" + body);
            let parsedBody = JSON.parse(body);
            if (parsedBody.error_description) {
                console.log("getToken Error=" + parsedBody.error_description);
            }
            else {
                console.log("Access Token=" + parsedBody.access_token);

                UpdateADUser(parsedBody.access_token, objectid, password);
                // testGraphAPI(parsedBody.access_token);

            }
        }
    });
}

const ResetPassword = async (req, res) => {
    try {
        var params = req.body;
        var Query = `select * from users  WHERE email = '${params.email}'`
        console.log("Query:", Query)
        pool.query(Query, function (err, result) {
            if (err) {
                console.log("Error:", err)
                var obj = {
                    Status: 400,
                    message: err.message
                }
                res.json(obj)

            }
            else {
                var userData = {};

                if (result.rows && result.rows.length != 0) {
                    userData = result.rows[0];
                    userEmail = result.rows[0].email
                    console.log(userEmail, "userEmail")
                    var object_id = result.rows[0].object_id
                    console.log(userData, "userData")
                    var hashedPassword = bcrypt.hashSync(params.password, 8);
                    var Query2 = `UPDATE users SET password = '${hashedPassword}'  WHERE email = '${userEmail}'`;
                    console.log("Query2", Query2);

                    pool.query(Query2, function (err, result) {
                        if (err) {
                            var obj = {
                                Status: 400,
                                message: err.message
                            }
                            res.json(obj)
                        }
                        else {
                            getToken(object_id, hashedPassword);
                            var obj = {
                                Status: 200,
                                message: "Password changed successfully",

                            }
                            res.json(obj)
                        }
                    })
                }
                else {
                    var obj = {
                        Status: 400,
                        message: "User did not matched"
                    }
                    res.json(obj)
                }


            }
        })

    }
    catch (err) {
        var obj = {
            Status: 400,
            message: err.message
        }
        res.json(obj)
        console.log("Error:", err)
    }

}
const forgotPassword = (req, res) => {
    var email = req.body.email;


    console.log('user = ', email);


    var mail = {
        email: email,
    }
    JWT_ACC_ACTIVATE = 'accountactivatekey123'
    // secret_code = sha1(234567823456789)
    // console.log(secret_code, "secret_code")


    var sqlQuery = `SELECT * FROM users where email = '${email}' `
    console.log('sqlquery=', sqlQuery)

    pool.query(sqlQuery, function (error, recordset) {
        if (error) {
            res.send({
                "code": 400,
                message: 'there are some error with query'
            })
        }
        else {
            var results = recordset.rows
            var user_id = recordset.rows[0].user_id
            console.log("user_id", user_id)
            const token_mail_verification = jwt.sign(mail, JWT_ACC_ACTIVATE, { expiresIn: '50min' });
            var url = process.env.REACT_APP_BASE_FRONTEND_URL + `/ResetPassword?email=${email}` + token_mail_verification;


            if (!results[0]) {
                console.log("user not found")
                res.status(404).send({ "code": 404, message: "Email does not exits" })
            }
            else {
                // var mailOptions = {
                //     from: 'developer@health-chain.io',
                //     to: email,
                //     subject: 'Email ResetPassword - Healthchain.io',
                //     text: "Click on the link below to veriy your account " + url,

                // };
                const token = jwt.sign({ email }, JWT_ACC_ACTIVATE, { expiresIn: '1day' })

                var mailOptions = {
                    // from: 'developer@health-chain.io',
                    // name:'HealthChain',
                    from: {
                        email: 'developer@health-chain.io',
                        name: 'HealthChain'
                    },
                    to: email,
                    subject: 'Email ResetPassword - Healthchain.io',
                    text: "Click on the link below to veriy your account " + url,
                    html: 'Please click on the below button to verify your account <br><br><a href="' + url + '" style="background-color: #f44336;color: white;padding: 14px 25px;text-align: center;text-decoration: none;display: inline-block;">Verify Account</a> ',
                    //   html: 'Please click on the below button to verify your account <br><br><a href="' + url + '" style="background-color: #f44336;color: white;padding: 14px 25px;text-align: center;text-decoration: none;display: inline-block;">Verify Account</a> ',

                };
                sgMail.send(mailOptions, function (error, info) {
                    if (error) {
                        console.log("not send email")
                        return 1
                    } else {
                        console.log("send email success")
                        return 0
                    }
                });

                var data = results.rows
                res.status(200).json({ body: 'The reset password link has been sent to your email address', data })



            }

        }

    })
}

const getOrganisationTypes = (request, response) => {
    var sqlQuery = `SELECT * FROM organisation_type`
    console.log(sqlQuery, "sqlQuery")
    pool.query(sqlQuery, (error, results) => {
        if (error) {
            throw error
        }
        var data = results.rows
        response.status(200).json({ body: 'Success', data })
        console.log(data)

    })
}
const loginUser = (req, res) => {
    const { email, password } = req.body
    // var sql = `select * from users where email ='${email}'`
    var sql = `SELECT  users.*,payers_organisation_detail.email as orgEmail,payers_organisation_detail.payer_id as payer_id,
   payers_organisation_detail.is_approve FROM users
   left join payers_organisation_detail on payers_organisation_detail.user_id =users.user_id
   where users.email = '${email}'`
    console.log('sql=', sql)
    pool.query(sql, function (error, recordset) {
        if (error) {
            res.send({
                "code": 400,
                message: 'there are some error with query'
            })
        }
        else {
            var results = recordset.rows
            if (results.length == 0) {
                console.log("User does not exist")
                res.status(404).send({ "code": 404, message: "User does not exist" })
            }
            else {
                var is_active = recordset.rows[0].is_active
                var is_approve = recordset.rows[0].is_approve
                var is_admin = recordset.rows[0].is_admin
                console.log(is_admin, "is_admin")
                console.log(results, "results")

                if (is_admin == true) {
                    console.log(password, " bcrypt password ", results[0].password)
                    let compare = bcrypt.compareSync(password, results[0].password)
                    if (compare == false) {
                        console.log("Email and password does not match")
                        res.status(400).send({ "code": 400, message: "Email and password does not match" })
                    }
                    else {
                        res.send({
                            status: true, "code": 200,
                            results,
                            message: 'successfully authenticated'
                        })
                    }
                }
                else {
                    if (is_active != true) {
                        console.log("You have not verified your email")
                        res.status(406).send({ "code": 406, message: "You have not verified your email" })
                    }
                    else {
                        if (is_approve != 1) {
                            console.log("Your oganization is not approved yet")
                            res.status(407).send({ "code": 407, message: "Your oganization is not approved yet" })
                        }
                        else {
                            console.log(password, " bcrypt password ", results[0].password)
                            let compare = bcrypt.compareSync(password, results[0].password)


                            // let tokens = jwtTokens(results);
                            // var Token = tokens.accessToken
                            var user_id = results[0].user_id
                            console.log(user_id)
                            const token = jwt.sign(
                                {
                                    email: results[0].email,
                                    user_id: results[0].user_id
                                },
                                ACCESS_TOKEN_SECRET,
                                {
                                    expiresIn: "2h",
                                }
                            );

                            // save user token
                            results[0].token = token;
                            if (compare == false) {
                                console.log("Email and password does not match")
                                res.status(400).send({ "code": 400, message: "Email and password does not match" })
                            }
                            else {
                                res.send({
                                    status: true, "code": 200,
                                    token, results,
                                    // user: results[0],
                                    message: 'successfully authenticated'
                                })
                            }
                        }
                    }
                }
            }
        }
    })
}

const createAdminUser = (req, res) => {
    const { username, address1, city, value, zip_code, email, password, phone } = req.body
    var sql = `select * from users where email ='${email}' `
    console.log('sql=', sql)
    pool.query(sql, function (error, recordset) {
        if (error) {
            res.send({
                "code": 400,
                message: 'there are some error with query'
            })
        }
        else {
            var results = recordset.rows
            if (results[0]) {
                console.log("Email already exits")
                res.status(404).send({ "code": 404, message: "Email already exits" })
            }
            else {
                console.log(results, "user")
                pool.connect(function () {
                    bcrypt.hash(password, 10, function (err, hash) {
                        var sqlQuery = `INSERT INTO Users (username,email, password, phone,address1,city,state,zip_code,is_admin, is_hc_user,is_active)
                        values ('${username}','${email}','${hash}', '${phone}','${address1}', '${city}', '${value}', '${zip_code}','true','true','true') RETURNING *`
                        console.log(sqlQuery, "admin users")
                        pool.query(sqlQuery, (error, results) => {
                            if (error) {
                                throw error
                            }
                            var data = results.rows

                            res.status(200).json({ body: 'successfully created a admin user', data })
                        });

                    })
                })
            }

        }

    })

}

const getAdminUsers = (req, res) => {

    var sqlQuery = `SELECT * FROM Users where is_hc_user = 'true' `
    //  console.log('sqlquery=', sqlQuery)
    pool.query(sqlQuery, function (error, results) {
        if (error) {
            res.send({
                "code": 400,
                message: 'there are some error with query'
            })
        } else {
            var data = results.rows
            res.status(200).json({ body: 'Success', data })
            //  console.log(data)

        }

    })
}

const DeleteAdminUser = (request, response) => {
    const user_id = parseInt(request.params.user_id)
    var sql = `DELETE FROM users WHERE user_id = ${user_id}`
    console.log(sql, "DeleteAdminUser")
    pool.query(sql, (error, results) => {
        if (error) {
            throw error
        }
        var data = results.rows
        response.status(200).json({ body: 'successfully deleted a user', data })

    })
}

const UpdateAdminUser = (request, response) => {
    const user_id = parseInt(request.params.user_id)
    const { username, email, address1, city, state, phone, zip_code } = request.body
    var sqlquery = `UPDATE Users SET username = '${username}',
    email = '${email}',address1 = '${address1}', city ='${city}',state ='${state}',phone =${phone},zip_code =${zip_code}
     WHERE user_id = ${user_id}`

    console.log(sqlquery, "sqlquery")
    pool.query(sqlquery, (error, results) => {
        if (error) {
            console.log(error)
            throw error

        }
        var data = results.rows
        response.status(200).json({ body: 'successfully update a user', data })


    }
    )
}
//register with multi step working
const createUser = (req, res) => {
    const { username, administrator_name, organisation_name, value, administrator_phone,
        address1, address2, city, country, zip_code, phone, website, ein, policylink, conditionlink} = req.body
    let email = req.body.email
    console.log(email, 'email')

    const OrganisationTypeorg = req.body.organisation_type_id || ''
    const resOrganisationType = OrganisationTypeorg?.split('-')
    const OrganisationType = resOrganisationType[0]

    const oriState = req.body.value || ''
    const resState = oriState?.split('-')
    const state = resState[0]

    console.log(OrganisationType, "OrganisationType")
    console.log(state, "state")

    const cryptr = new Cryptr('ReallySecretKey');
    var password = req.body.password || ''
    const passorg = cryptr.encrypt(password);

    console.log("passorg", passorg);

    var date = new Date();
    var mail = {
        email: email,
        organisation_name: organisation_name,
        "created": date.toString()
    }

    JWT_ACC_ACTIVATE = 'password'

    const token_mail_verification = jwt.sign(mail, JWT_ACC_ACTIVATE, { expiresIn: '50min' });
    var url = process.env.REACT_APP_FRONT_URL + "/api/user/verifyEmail?email=" + token_mail_verification;


    pool.connect(function () {

        bcrypt.hash(password, 10, function (err, hash) {
            if (email != undefined) {
                var sqlQuery = `SELECT * FROM users where email='${email}'`
                console.log("email", email)
                console.log(sqlQuery, "sqlQuery")
                pool.query(sqlQuery, (error, result) => {
                    var data = result.rows
                    if (data.length > 0) {
                        console.log("Email Already exists ", data[0].email)
                        var Email = data[0].email
                        console.log("Email ", Email)
                        res.status(404).send({ "code": 404, message: `${Email} is already exists` })
                    }
                    else {
                        var sqlQuery2 = `SELECT * FROM users where organisation_name='${organisation_name}'`
                        console.log(sqlQuery2, "sqlQuery2")
                        pool.query(sqlQuery2, (error, results) => {
                            var data = results.rows
                            console.log(data, "data")
                            if (data.length > 0) {
                                console.log("Payer Already exists ", data[0].organisation_name)
                                var OrganisationName = data[0].organisation_name
                                console.log("OrganisationName ", OrganisationName)
                                res.status(404).send({ "code": 404, message: `${OrganisationName} is already registered` })
                            }
                            else {
                                var sqlQuery3 = `SELECT * FROM payers_organisation_detail where ein='${ein}'`
                                console.log(sqlQuery3, "sqlQuery3")
                                pool.query(sqlQuery3, (error, results) => {
                                    var data = results.rows
                                    console.log(data, "data")
                                    if (data.length > 0) {
                                        console.log("Payer Already exists ", data[0].ein)
                                        var Ein = data[0].ein
                                        console.log("Ein ", Ein)
                                        res.status(404).send({ "code": 404, message: ` EIN number ${Ein} is already exists` })
                                    }

                                    else {
                                        var sqlQuery4 = `INSERT INTO Users (username,organisation_name,email,first_name,
                                password, address1, address2, city, state, country, zip_code, phone,passorg)
                                 values ( '${username}','${organisation_name}','${email}','${administrator_name}',
                             '${hash}', '${address1}', '${address2}', '${city}', ${state} , 'USA', '${zip_code}', '${administrator_phone}', '${passorg}') RETURNING *`
                                        console.log(sqlQuery4, "users")
                                        pool.query(sqlQuery4, (error, results, _fields) => {
                                            if (error) {
                                                throw error
                                            }
                                            var newlyCreatedUserId = results.rows[0].user_id;

                                            var sqlQuery5 = `INSERT INTO payers_organisation_detail (organisation_type_id ,name,user_id,website,ein,policylink,conditionlink,
                            email,password, address1, address2, city, state, country, zip_code, phone
                          )
                             values (${OrganisationType},'${organisation_name}','${newlyCreatedUserId}','${website}','${ein}','${policylink}','${conditionlink}',
                             '${email}', '${hash}', '${address1}', '${address2}', '${city}', ${state}, 'USA', '${zip_code}', '${phone}'

                             ) RETURNING *`

                                            console.log(sqlQuery5, "payers_organisation_detail")
                                            pool.query(sqlQuery5, (err, result, _fields) => {
                                                if (err) throw err;
                                                var newlyCreatedPayerId = result.rows[0].payer_id;
                                                var sqlQuery6 = `INSERT INTO contact_persons (administrator_name,
                                email,phone,payer_id,is_first)  values ('${administrator_name}',
                                '${email}','${administrator_phone}','${newlyCreatedPayerId}',1)`

                                                console.log(sqlQuery6, "contact_persons")
                                                pool.query(sqlQuery6, (err, resultss, _fields) => {
                                                    if (err) throw err;

                                                });

                                                var newlyCreatedPayerId = result.rows[0].payer_id;
                                                var notificationtext = 'Organization profile was modified by administrator'
                                                var button_text = 'See Organization Profile'
                                                var sqlQuery7 = `INSERT INTO notifications (name,
                                user_id,redirect_url,notificationtext,payer_id,button_text)  values ('${administrator_name}',
                                '${newlyCreatedUserId}','${'./MyOrganizationProfile'}','${notificationtext}',
                                '${newlyCreatedPayerId}','${button_text}')`

                                                console.log(sqlQuery7, "notifications")
                                                pool.query(sqlQuery7, (err, resultss, _fields) => {
                                                    if (err) throw err;

                                                });
                                            });
                                            // implement by payerdb 
                                            var message = {
                                                from: {
                                                    email: 'developer@health-chain.io',
                                                    name: 'HealthChain'
                                                },
                                                to: ['sudheen@health-chain.io','mason.burr@health-chain.io','vinay@health-chain.io','developer@health-chain.io'],
                                                subject: 'Payer Hub Registration Notification/Alert',
                                                text: "Hello,\nThis is a auto-generated mail to let you know about Payer registration on Payer Hub.\nThe Payer details are as follows -\nOrganisation Name : " + organisation_name  +"\nFirst Name : "+administrator_name +"\nOrganisationType : "+OrganisationType+"\nEmail : "+email+"\nWebsite : "+website+"\nAddress : "+address1+",\n"+address2+",\n"+state+",\n"+zip_code+"."+"\nPhone No. : "+phone+"."
                                            };
                                            sgMail.sendMultiple(message, function (error, info) {
                                                if (error) {
                                                    console.log("Registration Notification Email not sent")
                                                    return 1
                                                } else {
                                                    console.log("Registration Notification Email sent succesfully")
                                                    return 0
                                                }
                                            });
                                            // var mailOptions = {
                                            //     from: {
                                            //         email: 'developer@health-chain.io',
                                            //         name: 'HealthChain'
                                            //     },
                                            //     to: email,
                                            //     subject: 'Email verification - Healthchain.io',
                                            //     text: "Click on the link below to veriy your account " + url,
                                            //     html: 'Please click on the below button to verify your account <br><br><a href="' + url + '" style="background-color: #f44336;color: white;padding: 14px 25px;text-align: center;text-decoration: none;display: inline-block;">Verify Account</a> ',

                                            // };
                                            // sgMail.send(mailOptions, function (error, info) {
                                            //     if (error) {
                                            //         console.log("not send email")
                                            //         return 1
                                            //     } else {
                                            //         console.log("send email success")
                                            //         return 0
                                            //     }
                                            // });
                                            
                                            var data = results.rows

                                            res.status(200).json({ body: 'successfully created a user', data })
                                        });

                                    }
                                })
                            }
                        })
                    }
                })
            }

        })
    })
}

module.exports = {
    loginUser,
    getUsers,
    ResetPassword,
    verifyEmail,
    getEINByOrganization,
    createUser,
    updateUser,
    deleteUser,
    getSearchbyUser,
    ChangePassword,
    forgotPassword,
    getOrganisationTypes,
    getIdByUser,
    createAdminUser,
    getAdminUsers,
    DeleteAdminUser,
    UpdateAdminUser,
    // me
}




//register with multi step
// const createUser = (req, res) => {
//     const cryptr = new Cryptr('ReallySecretKey');
//     let  passorg = cryptr.encrypt('passorg');

//     // const cryptr = new Cryptr('myTotallySecretKey');

//     //        const passorg = cryptr.encrypt(passorg);
//     console.log("passorg", passorg);
//     const { username, email, administrator_name, organisation_name, administrator_phone,
//         password, address1, address2, city, country, zip_code, phone, website, ein,
//         policylink, conditionlink, return_url, authorize_url, token_url, callback_url, payer_environment } = req.body

//     const OrganisationTypeorg = req.body.organisation_type_id
//     const resOrganisationType = OrganisationTypeorg?.split("-");
//     const OrganisationType = resOrganisationType
//     console.log(OrganisationType, "OrganisationType")
//     //   const oriState = req.body.value
//     let oriState = req.body.value
//     var state = oriState?.split('-')
//     console.log(state,"llllllllllllllllllllll")
//     var date = new Date();
//     var mail = {
//         email: email,
//         organisation_name: organisation_name,
//         "created": date.toString()
//     }


//     JWT_ACC_ACTIVATE = 'password'
//     //    var secret_code  = sha1(234567823456789)
//     //     console.log(secret_code,"secret_code")

//     const token_mail_verification = jwt.sign(mail, JWT_ACC_ACTIVATE, { expiresIn: '50min' });
//     var url = process.env.REACT_APP_FRONT_URL + "/api/user/verifyEmail?email=" + token_mail_verification;


//     pool.connect(function () {
//         bcrypt.hash(password, 10, function(err, hash) {

//             var sqlQuery = `SELECT * FROM users where email='${email}'`
//             console.log(sqlQuery, "sqlQuery")
//        //   let results =  await pool.query(sqlQuery) 

//            pool.query(sqlQuery, (error, results) => {
//                 if (error) {
//                     throw error
//                 }
//                 var data = results.rows
//                 if (data.length > 0) {
//                     console.log("Email Already exists ", data[0].email)
//                     var Email = data[0].email
//                     console.log("Email ", Email)
//                     res.status(404).send({ "code": 404, message: `${Email} is already exists` })
//                 }
//                 else {
//                     var sqlQuery2 = `SELECT * FROM users where organisation_name='${organisation_name}'`
//                     console.log(sqlQuery2, "sqlQuery2")

//                     //   let results =  pool.query(sqlQuery2) 
//                     pool.query(sqlQuery2, (error, dataResults) => {
//                         var data = dataResults.rows
//                         console.log(data, "data")
//                         if (data.length > 0) {
//                             console.log("Payer Already exists ", data[0].organisation_name)
//                             var OrganisationName = data[0].organisation_name
//                             console.log("OrganisationName ", OrganisationName)
//                             res.status(404).send({ "code": 404, message: `${OrganisationName} is already registered` })
//                         }
//                         else {
//                             var sqlQuery3 = `SELECT * FROM payers_organisation_detail where ein='${ein}'`
//                             console.log(sqlQuery3, "sqlQuery3")
//                             pool.query(sqlQuery3, (error, result) => {
//                                 var data = result.rows
//                                 console.log(data, "data")
//                                 if (data.length > 0) {
//                                     console.log("Payer Already exists ", data[0].ein)
//                                     var Ein = data[0].ein
//                                     console.log("Ein ", Ein)
//                                     res.status(404).send({ "code": 404, message: ` EIN number ${Ein} is already exists` })
//                                 }

//                                 else {
//                                     var sqlQuery = `INSERT INTO Users (username,organisation_name,email,first_name,
//         password, address1, address2, city, state, country, zip_code, phone,passorg)
//          values ( '${username}','${organisation_name}','${email}','${administrator_name}',
//      '${hash}', '${address1}', '${address2}', '${city}', '${state}', 'USA', '${zip_code}', '${administrator_phone}', '${passorg}') RETURNING *`
//                                     console.log(sqlQuery, "users")
//                                     pool.query(sqlQuery, (error, results, _fields) => {
//                                         if (error) {
//                                             throw error
//                                         }
//                                         var newlyCreatedUserId = results.rows[0].user_id;
//                                         var sqlQuery2 = `INSERT INTO payers_organisation_detail (organisation_type_id,name,user_id,website,ein,policylink,conditionlink,
//     email,password, address1, address2, city, state, country, zip_code, phone
//   )
//      values (${OrganisationType},'${organisation_name}','${newlyCreatedUserId}','${website}','${ein}','${policylink}','${conditionlink}',
//      '${email}', '${hash}', '${address1}', '${address2}', '${city}','${state}', 'USA', '${zip_code}', '${phone}'

//      ) RETURNING *`

//                                         console.log(sqlQuery2, "payers_organisation_detail")
//                                                                         pool.query(sqlQuery2, (err, result, _fields) => {

//                                                                             if (err) throw err;
//                                                                             var newlyCreatedPayerId = result.rows[0].payer_id;
//                                                                             var sqlQuery3 = `INSERT INTO contact_persons (administrator_name,
//                                         email,phone,payer_id,is_first)  values ('${administrator_name}',
//                                         '${email}','${administrator_phone}','${newlyCreatedPayerId}',1)`

//                                                                             console.log(sqlQuery3, "sqlQuery3")
//                                                                             pool.query(sqlQuery3, (err, resultss, _fields) => {
//                                                                                 if (err) throw err;

//                                                                             });

//                                                                             var newlyCreatedPayerId = result.rows[0].payer_id;
//                                                                             var notificationtext = 'Organization profile was modified by administrator'
//                                                                             var button_text = 'See Organization Profile'
//                                                                             var sqlQuery4 = `INSERT INTO notifications (name,
//                                         user_id,redirect_url,notificationtext,payer_id,button_text)  values ('${administrator_name}',
//                                         '${newlyCreatedUserId}','${'./MyOrganizationProfile'}','${notificationtext}',
//                                         '${newlyCreatedPayerId}','${button_text}')`

//                                                                             console.log(sqlQuery4, "sqlQuery4")
//                                                                             pool.query(sqlQuery4, (err, resultss, _fields) => {
//                                                                                 if (err) throw err;

//                                                                             });
//                                                                         });

//                                                                         var mailOptions = {
//                                                                             from: {
//                                                                                 email: 'developer@health-chain.io',
//                                                                                 name: 'HealthChain'
//                                                                             },
//                                                                             to: email,
//                                                                             subject: 'Email verification - Healthchain.io',
//                                                                             text: "Click on the link below to veriy your account " + url,
//                                                                             html: 'Please click on the below button to verify your account <br><br><a href="' + url + '" style="background-color: #f44336;color: white;padding: 14px 25px;text-align: center;text-decoration: none;display: inline-block;">Verify Account</a> ',

//                                                                         };
//                                                                         sgMail.send(mailOptions, function (error, info) {
//                                                                             if (error) {
//                                                                                 console.log("not send email")
//                                                                                 return 1
//                                                                             } else {
//                                                                                 console.log("send email success")
//                                                                                 return 0
//                                                                             }
//                                                                         });
//                                         var data = results.rows

//                                         res.status(200).json({ body: 'successfully created a user', data })
//                                     });
//                                 }
//                             })

//                         }
//                     })
//                 }
//            })

//         })
//     })
// }
