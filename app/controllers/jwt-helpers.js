var jwt = require ('jsonwebtoken');
 var  ACCESS_TOKEN_SECRET ='padma'


function verifyToken(req, res, next)
{
  if (req.url != "/api/user/login") {
    if (
        !req.headers.authorization ||
        !req.headers.authorization.startsWith('Bearer') ||
        !req.headers.authorization.split(' ')[1]
    ) {
        return res.status(403).json({status_code: "403",
            message: "Please provide the token",
        });
    }
    const theToken = req.headers.authorization.split(' ')[1];
    if (theToken) {
        jwt.verify(theToken, ACCESS_TOKEN_SECRET, function (err, decoded) {
            if (err) {
                return res.json({ status_code: "401", message: "Invalid Access Token" });
            }
            req.decoded = decoded;
            next();

        })
    }

}
else {
    next();
}
}



module.exports = verifyToken


