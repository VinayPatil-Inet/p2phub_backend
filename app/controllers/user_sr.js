
var pool = require('../config/db.config')
// var User = require('./User')
var bcrypt = require("bcryptjs");
var crypto = require("crypto");
var jwtTokens = require("../controllers/jwt-helpers")
var randtoken = require('rand-token');
var nodemailer = require('nodemailer');
const jwt = require("jsonwebtoken");
//var sha1 = require('sha1');
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey('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');

const getUsers = (req, res) => {
    var sqlQuery = `SELECT  first_name||' '||last_name as name,
     phone, email, inserted_date, is_active, password ,user_id FROM Users`
    //  console.log('sqlquery=', sqlQuery)
    pool.query(sqlQuery, function (error, results) {
        if (error) {
            res.send({
                "code": 400,
                message: 'there are some error with query'
            })
        } else {
            var data = results.rows
            res.status(200).json({ body: 'Success', data })


        }

    })
}
const getIdByUser = (req, res) => {
    // 
    const id = parseInt(req.params.id)
    // var sqlQuery = `SELECT  users.*,name,payers_organisation_detail.email as org_email,payers_organisation_detail.payer_details as org_payer_details,
    // payers_organisation_detail.payer_url as org_payer_url,
    // payers_organisation_detail.payer_id as payer_id FROM users 
    // left join payers_organisation_detail on payers_organisation_detail.user_id =users.user_id
    // where payers_organisation_detail.user_id = ${id}`
    var sqlQuery = `SELECT * from users where user_id  = ${id}`

    console.log('sqlquery=', sqlQuery)
    pool.query(sqlQuery, function (error, results) {
        if (error) {
            res.send({
                "code": 400,
                message: 'there are some error with query'
            })
        } else {
            var data = results.rows
            res.status(200).json({ body: 'Success', data })
            //  console.log(data)

        }

    })
}
const getSearchbyUser = (req, res) => {
    var { last_name, first_name, email } = req.body

    pool.query("select * from users where last_name=$1 or first_name= $2 or email =$3"[last_name, first_name, email], (error, results) => {
        if (error) {
            throw error
        }

        var data = results.rows
        res.status(200).json({ body: 'Success', data })
        console.log(data)


    })


}
const createUser = (req, res) => {

    const { username, first_name, last_name, email, organisation_type_id, organisation_name, value,
        password, address1, address2, city, country, zip_code, phone } = req.body

    var date = new Date();
    var mail = {
        email: email,
        organisation_name: organisation_name,
        "created": date.toString()
    }

    JWT_ACC_ACTIVATE = 'password'
    //    var secret_code  = sha1(234567823456789)
    //     console.log(secret_code,"secret_code")

    const token_mail_verification = jwt.sign(mail, JWT_ACC_ACTIVATE, { expiresIn: '50min' });
    var url = "http://13.92.80.150:7000/api/user/verifyEmail?email=" + token_mail_verification;


    pool.connect(function () {

        bcrypt.hash(password, 10, function (err, hash) {

            var sqlQuery = `SELECT * FROM users where email='${email}'`
            console.log(sqlQuery, "sqlQuery")
            pool.query(sqlQuery, (error, results) => {
                if (error) {
                    throw error
                }
                var data = results.rows
                if (data.length > 0) {
                    console.log("Email Already exists ", data)
                    res.status(400).json({ body: 'Email Already exists', data })
                }
                else {
                    var sqlQuery = `INSERT INTO Users (username,organisation_name,email,first_name, last_name,
                                password, address1, address2, city, state, country, zip_code, phone) 
                                 values ( '${username}','${organisation_name}','${email}','${first_name}','${last_name}',
                             '${hash}', '${address1}', '${address2}', '${city}', '${value}', 'USA', '${zip_code}', '${phone}') RETURNING *`
                    console.log(sqlQuery, "users")
                    pool.query(sqlQuery, (error, results, _fields) => {
                        if (error) {
                            throw error
                        }
                        var newlyCreatedUserId = results.rows[0].user_id;

                        var sqlQuery2 = `INSERT INTO payers_organisation_detail (organisation_type_id,name,user_id) 
                             values (${organisation_type_id},'${organisation_name}','${newlyCreatedUserId}') RETURNING *`

                        pool.query(sqlQuery2, (err, result, _fields) => {
                            if (err) throw err;
                            var newlyCreatedPayerId = result.rows[0].payer_id;
                            var sqlQuery3 = `INSERT INTO contact_persons (first_name, last_name,
                                email,phone,payer_id,is_first)  values ('${first_name}','${last_name}',
                                '${email}','${phone}','${newlyCreatedPayerId}',1)`
                            pool.query(sqlQuery3, (err, resultss, _fields) => {
                                if (err) throw err;

                            });
                        });
                        var mailOptions = {
                            from: {
                                email: 'developer@health-chain.io',
                                name: 'HealthChain'
                            },
                            to: email,
                            subject: 'Email verification - Healthchain.io',
                            text: "Click on the link below to veriy your account " + url,
                            html: 'Please click on the below button to verify your account <br><br><a href="' + url + '" style="background-color: #f44336;color: white;padding: 14px 25px;text-align: center;text-decoration: none;display: inline-block;">Verify Account</a> ',

                        };
                        sgMail.send(mailOptions, function (error, info) {
                            if (error) {
                                console.log("not send email")
                                return 1
                            } else {
                                console.log("send email success")
                                return 0
                            }
                        });

                        var data = results.rows

                        res.status(200).json({ body: 'successfully created a user', data })
                    });
                }
                // response.status(200).json({ body: 'Success', data })
                // console.log(data)

            })



        })
    })
}
const verifyEmail = (req, res) => {
    // var secret_code  = sha1(234567823456789)
    // console.log(secret_code,"secret_code")
    token = req.query.email;
    console.log(token)
    if (token) {
        try {
            jwt.verify(token, JWT_ACC_ACTIVATE, (e, decoded) => {
                if (e) {
                    console.log(e)
                    res.send({ code: 403, message: "Expired link. Signup again" })
                    console.log("Expired link. Signup again", e);
                } else {
                    var email = decoded.email;
                    pool.connect(function () {
                        var query = `UPDATE  users  SET  is_active = 'true'  WHERE  email = '${email}' RETURNING *`
                        pool.query(query, function (err, results, _fields) {
                            var data = results.rows
                            console.log(query, "query")
                            if (err) {
                                //   console.log(response.data.data[0].payer_signed_status_id,"payer_signed_status_id")
                                console.log(err);
                                console.log("Please try again!");
                                res.send("Please try again!");
                            } else {
                                console.log("updated Successfully");
                                console.log(data, "Successfully");
                                var userId = results.rows[0].user_id
                                console.log(userId, "userId");
                                var sql = `select  name,inserted_by, payer_id,
                              users.user_id, organisation_type_id
                          FROM payers_organisation_detail
                          left join users on users.user_id =payers_organisation_detail.user_id where
                          users.user_id=${userId}`
                                pool.query(sql, function (err, result) {
                                    if (err)
                                        console.log(err);
                                    else {
                                        data = result.rows
                                        var payerId = result.rows[0].payer_id
                                        console.log(payerId, "payerId")
                                        res.redirect(`http://13.92.80.150:3000/newOrganization?userId=${userId}/payerId=${payerId}?success=` + encodeURIComponent('You have successfuly verified email'));
                                    }


                                })
                               
                            }
                          
                        });
                    });
                }
            });
        } catch (err) {

            res.send({ code: 403, message: "Expired link. Signup again" })
            console.log("Expired link. Signup again", err);
        }
    } else {
        res.send({ code: 403, message: "Expired link. Signup again" })
    }




}
const updateUser = (request, response) => {
    const id = parseInt(request.params.id)
    const { phone, email, inserted_date, first_name, last_name, user_types_id, is_active } = request.body
    var sqlquery = `UPDATE Users SET phone = $1, email = $2, inserted_date = $3,first_name =$4, last_name = $5, user_types_id= $6, is_active = $7 WHERE id = $8`
    pool.query(sqlquery, [phone, email, inserted_date, first_name, last_name, user_types_id, is_active, id],
        (error, results) => {
            if (error) {
                console.log(error)
                throw error

            }
            response.status(200).json({ body: 'successfully update a user' })


        }
    )
}

const loginUser = (req, res) => {
    var email = req.body.email;
    var password = req.body.password;
    console.log('user = ', email);
    var sqlQuery = `SELECT  users.*,payers_organisation_detail.email as orgEmail,payers_organisation_detail.payer_id as payer_id FROM users  left join payers_organisation_detail on payers_organisation_detail.user_id =users.user_id
    where users.email = '${email}' and  is_active = 'true' and (payers_organisation_detail.is_approve=1 or is_admin='true')`

    // var sqlQuery =`SELECT  users.*,payers_organisation_detail.email as org_email,
    // payers_organisation_detail.address1 as org_address1,
    //  payers_organisation_detail.address2 as org_address2,
    //   payers_organisation_detail.name,
    // payers_organisation_detail.city as org_city,payers_organisation_detail.state as org_state,
    // payers_organisation_detail.phone as org_phone,payers_organisation_detail.postal_code as org_postal_code,
    // payers_organisation_detail.payer_id as payer_id
    //  FROM users  left join payers_organisation_detail on payers_organisation_detail.user_id =users.user_id
    // where users.email = '${email}' and  is_active = 'true'`
    console.log('sqlquery=', sqlQuery)
    pool.query(sqlQuery, function (error, recordset) {
        if (error) {
            res.send({
                "code": 400,
                message: 'there are some error with query'
            })
        }
        else {
            var results = recordset.rows
            let tokens = jwtTokens(results);
            var Token = tokens.accessToken

            if (!results[0]) {
                console.log("user not found")
                res.status(404).send({ "code": 404, message: "Email does not exits" })
            }
            else {
                console.log(password, " bcrypt password ",results[0].password)
                let compare = bcrypt.compareSync(password, results[0].password)
                if (compare == false) {
                    res.status(400).send({ "code": 400, message: "Email and password does not match" })
                }
                else {
                    res.send({
                        status: true, "code": 200,
                        results, Token,
                        message: 'successfully authenticated'
                    })
                }

            }

        }

    })
}

const deleteUser = (request, response) => {
    const id = parseInt(request.params.id)
    pool.query('DELETE FROM users WHERE user_id = $1', [id], (error, results) => {
        if (error) {
            throw error
        }
        response.status(200).json({ body: 'successfully deleted a user' })

    })
}
const resetPassword = async (req, res) => {
    try {
        var params = req.body;
        var Query = `select * from users where email = '${params.email}'`
        pool.query(Query, function (err, result) {
            if (err) {
                console.log("Error:", err)
                var obj = {
                    Status: 400,
                    message: err.message
                }
                res.json(obj)

            }
            else {
                var userData = {};

                if (result.rows && result.rows.length != 0) {
                    userData = result.rows[0];
                    console.log(userData, "userData")
                    var hashedPassword = bcrypt.hashSync(params.password, 8);
                    var Query2 = `UPDATE users SET password = '${hashedPassword}'  WHERE email = '${params.email}'`;
                    console.log("Query2", Query2);

                    pool.query(Query2, function (err, result) {
                        if (err) {
                            var obj = {
                                Status: 400,
                                message: err.message
                            }
                            res.json(obj)
                        }
                        else {
                            var obj = {
                                Status: 200,
                                message: "Password changed successfully",

                            }
                            res.json(obj)
                        }
                    })
                }
                else {
                    var obj = {
                        Status: 400,
                        message: "User did not matched"
                    }
                    res.json(obj)
                }


            }
        })

    }
    catch (err) {
        var obj = {
            Status: 400,
            message: err.message
        }
        res.json(obj)
    }

}

const forgotPassword = (req, res) => {
    var email = req.body.email;

    console.log('user = ', email);

    // var mail = {
    //     email: email,
    // }
    // JWT_ACC_ACTIVATE = 'accountactivatekey123'
    secret_code = sha1(234567823456789)
    console.log(secret_code, "secret_code")

    const token_mail_verification = jwt.sign(mail, JWT_ACC_ACTIVATE, { expiresIn: '50min' });
    var url = "http://13.92.80.150:3000/ResetPassword?=" + token_mail_verification;

    var sqlQuery = `SELECT * FROM users where email = '${email} `
    console.log('sqlquery=', sqlQuery)

    pool.query(sqlQuery, function (error, recordset) {
        if (error) {
            res.send({
                "code": 400,
                message: 'there are some error with query'
            })
        }
        else {
            var results = recordset.rows
            if (!results[0]) {
                console.log("user not found")
                res.status(404).send({ "code": 404, message: "Email does not exits" })
            }
            else {
                // var mailOptions = {
                //     from: 'developer@health-chain.io',
                //     to: email,
                //     subject: 'Email ResetPassword - Healthchain.io',
                //     text: "Click on the link below to veriy your account " + url,

                // };
                const token = jwt.sign({ email }, secret_code, { expiresIn: '1day' })

                var mailOptions = {
                    // from: 'developer@health-chain.io',
                    // name:'HealthChain',
                    from: {
                        email: 'developer@health-chain.io',
                        name: 'HealthChain'
                    },
                    to: email,
                    subject: 'Email ResetPassword - Healthchain.io',
                    text: "Click on the link below to veriy your account " + url,
                    html: 'Please click on the below button to verify your account <br><br><a href="' + url + '" style="background-color: #f44336;color: white;padding: 14px 25px;text-align: center;text-decoration: none;display: inline-block;">Verify Account</a> ',

                };
                sgMail.send(mailOptions, function (error, info) {
                    if (error) {
                        console.log("not send email")
                        return 1
                    } else {
                        console.log("send email success")
                        return 0
                    }
                });

                var data = results.rows
                res.status(200).json({ body: 'The reset password link has been sent to your email address', data })



            }

        }

    })
}

const getOrganisationTypes = (request, response) => {
    var sqlQuery = `SELECT * FROM organisation_type`
    console.log(sqlQuery, "sqlQuery")
    pool.query(sqlQuery, (error, results) => {
        if (error) {
            throw error
        }
        var data = results.rows
        response.status(200).json({ body: 'Success', data })
        console.log(data)

    })
}

const createAdminUser = (req, res) => {
    const { username, address1, city, value, zip_code, email, password, phone } = req.body
    var sql = `select * from users where email ='${email}'`
    console.log('sql=', sql)
    pool.query(sql, function (error, recordset) {
        if (error) {
            res.send({
                "code": 400,
                message: 'there are some error with query'
            })
        }
        else {
            var results = recordset.rows
            if (results[0]) {
                console.log("Email already exits")
                res.status(404).send({ "code": 404, message: "Email already exits" })
            }
            else {
                console.log(results, "user")
                pool.connect(function () {
                    bcrypt.hash(password, 10, function (err, hash) {
                        var sqlQuery = `INSERT INTO Users (username,email, password, phone,address1,city,state,zip_code,is_admin, is_hc_user,is_active) 
                        values ('${username}','${email}','${hash}', '${phone}','${address1}', '${city}', '${value}', '${zip_code}','false','true','true') RETURNING *`
                        console.log(sqlQuery, "admin users")
                        pool.query(sqlQuery, (error, results) => {
                            if (error) {
                                throw error
                            }
                            var data = results.rows

                            res.status(200).json({ body: 'successfully created a admin user', data })
                        });

                    })
                })
            }

        }

    })

}

const getAdminUsers = (req, res) => {

    var sqlQuery = `SELECT * FROM Users where is_hc_user = 'true' `
    //  console.log('sqlquery=', sqlQuery)
    pool.query(sqlQuery, function (error, results) {
        if (error) {
            res.send({
                "code": 400,
                message: 'there are some error with query'
            })
        } else {
            var data = results.rows
            res.status(200).json({ body: 'Success', data })
            //  console.log(data)

        }

    })
}

module.exports = {
    loginUser,
    getUsers,
    verifyEmail,
    createUser,
    updateUser,
    deleteUser,
    getSearchbyUser,
    resetPassword,
    forgotPassword,
    getOrganisationTypes,
    getIdByUser,
    createAdminUser,
    getAdminUsers

}


