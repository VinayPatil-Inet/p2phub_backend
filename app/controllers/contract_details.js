var pool = require('../config/db.config')

const getContractDetails = (request, response) => {
    pool.query('SELECT * FROM contract_details', (error, results) => {
        if (error) {
            throw error
        }
        var data = results.rows
        response.status(200).json({ body: 'Success', data })
        console.log(data)

    })
}



const createContractDetails = (request, response) => {
    const { id, contractee_payer_id, contractor_payer_id, inserted_date, updated_date, contract_file_path, inserted_by, status, is_active, is_void } = request.body
    pool.query('INSERT INTO contract_details (id, contractee_payer_id, contractor_payer_id, inserted_date, updated_date, contract_file_path, inserted_by, status, is_active, is_void) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)', [id, contractee_payer_id, contractor_payer_id, inserted_date, updated_date, contract_file_path, inserted_by, status, is_active, is_void], (error, results) => {
        if (error) {
            throw error
        }
        response.status(200).json({ body: 'successfully created a contract details' })
        // console.log(data)

    })
}
const updateContractDetails = (request, response) => {
    const id = parseInt(request.params.id)
    const { contractee_payer_id, contractor_payer_id, inserted_date, updated_date, contract_file_path, inserted_by, status, is_active, is_void } = request.body
    pool.query(
        "UPDATE contract_details SET contractee_payer_id = $1, contractor_payer_id = $2, inserted_date = $3,updated_date =$4, contract_file_path = $5, inserted_by= $6, status = $7, is_active = $8, is_void = $9 WHERE id = $10",
        [contractee_payer_id, contractor_payer_id, inserted_date, updated_date, contract_file_path, inserted_by, status, is_active, is_void, id],
        (error, recoredset) => {
            if (error) {
                throw error
            }
            response.status(200).json({ body: 'successfully update a contract details' })

        }
    )
}
const deleteContractDetails = (request, response) => {
    const id = parseInt(request.params.id)
    pool.query('DELETE FROM contract_details WHERE id = $1', [id], (error, results) => {
        if (error) {
            throw error
        }
        response.status(200).json({ body: 'successfully deleted a contract details' })

    })
}

const createContact = (req,res) =>{
    var { name, email, phone_number, message} = req.body
    var sqlQuery = `INSERT INTO contact (name,email, phone_number,message)
    values ('${name}','${email}','${phone_number}', '${message}') RETURNING *`
    console.log(sqlQuery, "Contact")
    pool.query(sqlQuery, (error, results) => {
        if (error) {
            throw error
        }
        var data = results.rows

        res.status(200).json({ body: 'successfully created a admin user', data })
    });

}


module.exports = {
    createContact,
    getContractDetails,
    createContractDetails,
    updateContractDetails,
    deleteContractDetails
}










