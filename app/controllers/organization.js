var pool = require('../config/db.config')
var bcrypt = require("bcryptjs");
const sgMail = require('@sendgrid/mail');
const requestad = require("request");
const Cryptr = require('cryptr');
const fs = require("fs");
const fastcsv = require("fast-csv");
const { cls } = require('sequelize');
sgMail.setApiKey('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');

const getOrganisationId = (request, response) => {
    const value = parseInt(request.params.value)
    var sql = `SELECT p.payer_id as value, p.name,p.admin_payer_id, p.is_approve, p.logo,p.email,p.phone, p.address1, p.address2, p.city,
    p.state,p.user_id, p.type,p.website, p.policylink, p.conditionlink, p.zip_code, location.state_name as state,users.is_admin,
     p.postal_code, p.organisation_type_id,organisation_type.organisation_type,
    end_point_master.endpoint_name, payer_end_points.base_url,payer_end_points.auth_scope,payers_signed_status.payer_status
    FROM payers_organisation_detail p
	LEFT JOIN payer_end_points ON payer_end_points.inserted_by = p.user_id
	LEFT JOIN payer_contact ON payer_contact.inserted_by = p.user_id
	LEFT JOIN payers_signed_status ON payers_signed_status.destinatoin_payer_id = p.payer_id
    LEFT JOIN payer_end_point_mapping ON payer_end_point_mapping.payer_endpoint_id = payer_end_points.id
	LEFT JOIN end_point_master ON end_point_master.id = payer_end_point_mapping.endpoint_master_id
	LEFT JOIN users ON users.user_id =p.user_id
	 left join organisation_type on organisation_type.id =p.organisation_type_id
	 left join location on location.id =p.state
	where p.payer_id =${value} FETCH FIRST 1 ROWS ONLY`

    console.log(sql, "sql")

    pool.query(sql, (error, results) => {
        if (error) {
            throw error
        }


        var data = results.rows
        console.log(data, "data")
        response.status(200).json({ body: 'Success', data })


    })
}

const getAdminOrganisationId = (request, response) => {
    const value = parseInt(request.params.value)
    var sql = `SELECT p.admin_payer_id as value, p.org_payer_id,p.name, p.is_approve,p.email,p.phone, p.address1, p.address2, p.city,
    p.state,p.user_id,p.website, p.policylink, p.conditionlink, p.zip_code,organisation_type.organisation_type as type,location.state_name as state,
    end_point_master.endpoint_name, payer_end_points.base_url,payer_end_points.auth_scope,payer_end_points.other_endpoint_name,payers_signed_status.payer_status
    FROM admin_payers_organisation_detail p
	LEFT JOIN payer_end_points ON payer_end_points.inserted_by = p.user_id
	LEFT JOIN payer_contact ON payer_contact.inserted_by = p.user_id
	LEFT JOIN admin_payers_organisation_detail ON admin_payers_organisation_detail.user_id = p.user_id
	LEFT JOIN payers_signed_status ON payers_signed_status.destinatoin_payer_id = p.admin_payer_id
    LEFT JOIN payer_end_point_mapping ON payer_end_point_mapping.payer_endpoint_id = payer_end_points.id
	LEFT JOIN end_point_master ON end_point_master.id = payer_end_point_mapping.endpoint_master_id
    LEFT JOIN organisation_type ON organisation_type.id = p.type
    LEFT JOIN location on location.id =p.state
	where p.admin_payer_id =${value} FETCH FIRST 1 ROWS ONLY`
    console.log(sql, "sql")

    pool.query(sql, (error, results) => {
        if (error) {
            throw error
        }


        var data = results.rows
        response.status(200).json({ body: 'Success', data })

    })
}

const getAllOrganisation = (request, response) => {

    pool.query(`SELECT p.payer_id, p.user_id,p.name, location.state_name,p.email,p.username,p.password,p.phone, p.address1,p.address2,p.city,p.state,p.zip_code,
    p.updated_date, p.inserted_by,p.is_approve,organisation_type as type,users.is_admin,p.admin_payer_id,
   p.conditionlink,p.policylink,p.country,p.logo,p.fax,p.ein,p.website
   FROM payers_organisation_detail p
              left join location on location.id = p.state
                  left join users on users.user_id =p.user_id
                  left join organisation_type on organisation_type.id =p.organisation_type_id where p.email!=''
                  ORDER BY payer_id DESC`, (error, results) => {
        if (error) {
            throw error
        }

        var data = results.rows
        // response.status(200).send(`User deleted with ID: ${id}`)
        response.status(200).json({ body: 'Success', data })

    })
}

const getAllAdminOrganisation = (request, response) => {

    pool.query(`SELECT p.admin_payer_id, location.state_name,p.admin_payer_id as value,p.user_id,p.name as label,p.name, p.email,p.username,p.password,p.phone, p.address1,p.address2,p.city,p.state,p.zip_code,
    p.updated_date, p.inserted_by,organisation_type as type,users.is_admin,payers_organisation_detail.is_approve,
   p.conditionlink,p.policylink,p.country,p.logo,p.fax,p.ein,p.website
   FROM admin_payers_organisation_detail p
                 left join location on location.id = p.state
                  left join users on users.user_id =p.user_id
				    left join payers_organisation_detail on payers_organisation_detail.admin_payer_id =p.admin_payer_id
                  left join organisation_type on organisation_type.id =p.type
                  ORDER BY admin_payer_id DESC`, (error, results) => {
        if (error) {
            throw error
        }

        var data = results.rows
        // response.status(200).send(`User deleted with ID: ${id}`)
        response.status(200).json({ body: 'Success', data })

    })
}

const createOrganisation = (request, response) => {
    const { name, email, phone, address1, address2, city, state, postal_code, payer_details, payer_url, end_point, inserted_date, updated_date, inserted_by, user_id } = request.body
    pool.query('INSERT INTO payers_organisation_detail (name, email, phone, address1, address2, city, state, postal_code, payer_details, payer_url, end_point, inserted_date, updated_date, inserted_by,  user_id) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10,$11, $12, $13, $14, $15)', [name, email, phone, address1, address2, city, state, postal_code, payer_details, payer_url, end_point, inserted_date, updated_date, inserted_by, user_id], (error, results) => {
        if (error) {
            throw error
        }
        response.status(200).json({ body: 'successfully created a organization' })
        //  console.log(data)

    })
}

const updateOrganisation = (request, response) => {
    const id = parseInt(request.params.id)
    const stateid = parseInt(request.params.stateid)
    const { email, phone, address1, address2, city, first_name2,

        last_name2, email2, phone2, postal_code, payer_details } = request.body


    // var state = request.body.label

    pool.query(
        "UPDATE payers_organisation_detail SET  email = $1, phone = $2,address1 =$3, address2 = $4, city= $5, state = $6, postal_code =$7, payer_details =$8 WHERE payer_id = $9",
        [email, phone, address1, address2, city, stateid, postal_code, payer_details, id],
        (error, recoredset) => {
            if (error) {
                throw error
            }
            pool.query("INSERT INTO contact_persons (first_name,last_name,email,phone,payer_id) VALUES ($1,$2,$3,$4,$5)", [first_name2, last_name2, email2, phone2, id], (err, results) => {

                if (err) throw err;

            });

            response.status(200).json({ body: 'successfully update a organization' })

        }
    )
}

const deleteOrganisation = (request, response) => {
    const id = parseInt(request.params.id)
    pool.query('DELETE FROM payers_organisation_detail WHERE id = $1', [id], (error, results) => {
        if (error) {
            throw error
        }
        // response.status(200).send(`User deleted with ID: ${id}`)
        response.status(200).json({ body: 'successfully deleted a payer organization' })


    })
}

const getOrganisationByUserId = (request, response) => {
    const id = parseInt(request.params.id)

    pool.query('SELECT  payers_organisation_detail.payer_id, name,first_name,last_name,contact_persons.email,contact_persons.phone FROM payers_organisation_detail left join contact_persons on contact_persons.payer_id=payers_organisation_detail.payer_id WHERE user_id = $1 and is_first=1', [id], (error, results) => {

        if (error) {
            throw error
        }
        var data = results.rows
        response.status(200).json({ body: 'Success', data })
        console.log(data)

    })
}

const getBothPayers = (request, response) => {

    const statusid = request.params.statusid;
    console.log("statusid==", statusid);

    var sql = `SELECT source_payer_id ,destinatoin_payer_id FROM payers_signed_status WHERE payer_signed_status_id = '${statusid}'`
    console.log(sql, "getBothPayers")
    pool.query(sql, (error, result) => {
        if (error) {
            throw error
        }
        console.log("payers_signed_status payer details== ", result.rows);
        var sourceid = result.rows[0].source_payer_id;
        var destid = result.rows[0].destinatoin_payer_id;
        console.log("sourceid----- ", sourceid, destid)
        pool.query('select a.payer_id as spayerid, a.name sname, a.email semail, a.payer_url spayerurl,b.payer_id dpayerid, b.name dname, b.email demail, b.payer_url dpayerurl from payers_organisation_detail inner join payers_organisation_detail a on a.payer_id = $1  inner join payers_organisation_detail b on b.payer_id = $2 limit 1', [sourceid, destid], (error, results) => {
            //  pool.query('SELECT  payers_organisation_detail.payer_id, name,first_name,last_name,contact_persons.email,contact_persons.phone FROM payers_organisation_detail left join contact_persons on contact_persons.payer_id=payers_organisation_detail.payer_id WHERE users_id = 2', (error, results) => {
            if (error) {
                throw error
            }
            var data = results.rows
            response.status(200).json({ body: 'Success', data })
            //  console.log("both payer details== ", data)

        })

    })

}

const getOrganisationById = (request, response) => {
    const id = parseInt(request.params.id)

    pool.query('SELECT  id, name, email, phone, address1, address2, city, state, postal_code, payer_details FROM payers_organisation_detail WHERE id = $1', [id], (error, results) => {
        if (error) {
            throw error
        }
        var data = results.rows
        response.status(200).json({ body: 'Success', data })
        console.log(data)

    })
}

const Organisation = (request, response) => {
    const name = request.params.payername;
    const label = request.params.location;
    const organizationtype = request.params.organizationtype;
    const value = parseInt(request.params.payerid)
    console.log(name, "name")
    console.log(organizationtype, "organizationtype")

    var labelArr = label.split(',');
    console.log(labelArr, "labelArr")
    const labelFinal = labelArr.map(c => `'${c}'`).join(', ');
    var organizationtypeArr = organizationtype.split(',');
    console.log(organizationtypeArr, "organizationtypeArr")
    const organizationtypeFinal = organizationtypeArr.map(c => `'${c}'`).join(', ');

    console.log(labelFinal, organizationtypeFinal, "labelFinal")

    var sqlquery = `SELECT payer_id as value ,payers_organisation_detail.state as label,location.state_name,name,first_name AS fullname, users.is_admin,
     organisation_type, payers_organisation_detail.email, payers_organisation_detail.phone, payers_organisation_detail.address1, payers_organisation_detail.address2, payers_organisation_detail.city,
    postal_code, payer_details, payer_url,source_payer_id,destinatoin_payer_id, end_point, payers_organisation_detail.inserted_by, 
    payers_organisation_detail.user_id,payers_organisation_detail.updated_date FROM payers_organisation_detail
    left JOIN  (SELECT DISTINCT ON (source_payer_id) * FROM   payers_signed_status) m ON m.source_payer_id =
			   payers_organisation_detail.payer_id

               left join location on location.id = payers_organisation_detail.state
               left join users on users.user_id =payers_organisation_detail.user_id
               left join organisation_type on organisation_type.id =payers_organisation_detail.organisation_type_id
               where payers_organisation_detail.payer_id!= ${value} and
                payers_organisation_detail.payer_id not in(select destinatoin_payer_id from payers_signed_status where
                    source_payer_id=${value} and payer_status !='null') and is_approve=1 and is_active ='true' `

    if (name == 'All' && label == 'All' && organizationtype == 'All') {
        sqlquery = sqlquery;
    }
    else if (name != 'All' && label == 'All' && organizationtype == 'All') {
        sqlquery = sqlquery + ` and  Lower(name) like Lower('${name}%')  `

    }
    else if (name == 'All' && label != 'All' && organizationtype == 'All') {
        sqlquery = sqlquery + ` and payers_organisation_detail.state in(${labelFinal})`
    }
    else if (name == 'All' && label == 'All' && organizationtype != 'All') {
        sqlquery = sqlquery + ` and organisation_type_id in(${organizationtypeFinal})`
    }
    else if (name != 'All' && label != 'All' && organizationtype == 'All') {
        sqlquery = sqlquery + ` and payers_organisation_detail.state in(${labelFinal}) and  name like '${name}%'`
    }
    else if (name == 'All' && label != 'All' && organizationtype != 'All') {
        sqlquery = sqlquery + ` and payers_organisation_detail.state in(${labelFinal}) and organisation_type_id  in(${organizationtypeFinal})`
    }
    else {
        sqlquery = sqlquery + ` and payers_organisation_detail.state in(${labelFinal}) and  name like '${name}%'  and organisation_type_id  in(${organizationtypeFinal})`
    }

    console.log('sql query ppppp = ', sqlquery);
    pool.query(sqlquery, function (err, results) {

        if (err) {
            response.status(500).send("Error in execute query");
        }
        else {
            var data = results.rows
            console.log(data, "data")
            response.status(200).json({ body: 'Success', data })
        }
    })
}

const getOrganisationAddMyList = (request, response) => {
    const value = parseInt(request.params.value)
    var sql = `SELECT payers_organisation_detail.payer_id,name, email, organisation_type,user_id,payer_signed_status_id, source_payer_id,
    destinatoin_payer_id, contract_file_path, payer_status
        FROM  payers_signed_status
        LEFT JOIN payers_organisation_detail  ON payers_signed_status.destinatoin_payer_id = payers_organisation_detail.payer_id
        left join organisation_type on organisation_type.id =payers_organisation_detail.organisation_type_id
          where  payers_signed_status.source_payer_id= ${value}
     and payer_status ='Signed' `
    console.log(sql, "sql")
    pool.query(sql, (error, results) => {
        if (error) {
            throw error
        }

        var data = results.rows
        response.status(200).json({ body: 'Success', data })
        console.log(data)

    })
}

const getStates = (request, response) => {

    var sql = `SELECT id as value,state_name as label,short_name from location`
    // console.log(sql, "sql")
    // const userdetails = {
    //     "displayName": "Ramesh",
    //     "identities": [
    //         {
    //             "signInType": "emailAddress",
    //             "issuer": "healthchainB2c.onmicrosoft.com",
    //             "issuerAssignedId": "ramesh@gmail.com"
    //         }
    //     ],
    //     "passwordProfile": {
    //         "password": "Vinay@123",
    //         "forceChangePasswordNextSignIn": false
    //     },
    //     "passwordPolicies": "DisablePasswordExpiration"
    // };
    // getToken(userdetails,2,90);

    pool.query(sql, (error, results) => {
        if (error) {
            throw error
        }
        var data = results.rows
        response.status(200).json({ body: 'Success', data })


    })
}

const getPayersEnvironment = (request, response) => {

    var sql = `SELECT  id ,payer_environment  from payers_environment`
    pool.query(sql, (error, results) => {
        if (error) {
            throw error
        }
        var data = results.rows
        response.status(200).json({ body: 'Success', data })


    })
}

const getMyOrganizationProfile = (request, response) => {
    const value = parseInt(request.params.value)
    var sql =
        `SELECT payer_id as value ,payers_organisation_detail.state as label,organisation_type_id,name,first_name AS fullname,website,ein,policylink,conditionlink,
        payers_organisation_detail.zip_code,logo,organisation_display_name,api_url_production,app_url_pre_production,primary_developer_email,
        primary_developer_title,primary_developer_name,admin_payer_id,
         users.username,users.password,users.is_admin, users.first_name,users.last_name,
        organisation_type,users.phone as userphone,users.email as useremail, location.state_name,payers_organisation_detail.email, payers_organisation_detail.phone, payers_organisation_detail.address1, payers_organisation_detail.address2, payers_organisation_detail.city,
       postal_code, payer_details, payer_url,users.country,
       payers_organisation_detail.inserted_by, payers_organisation_detail.user_id,payers_organisation_detail.updated_date FROM payers_organisation_detail
                  left join users on users.user_id =payers_organisation_detail.user_id
                    left join location on location.id =payers_organisation_detail.state
                  
                  left join organisation_type on organisation_type.id =payers_organisation_detail.organisation_type_id
                  where payers_organisation_detail.payer_id=${value}`
                  //  left join payers_environment on payers_environment.id =payers_organisation_detail.payer_environment
                  //payers_organisation_detail.payer_environment as payer_environment_id,payers_environment.payer_environment,
    console.log(sql, "sql")
    pool.query(sql, (error, results) => {
        if (error) {
            throw error
        }
        var data = results.rows
        response.status(200).json({ body: 'Success', data })
        console.log("getMyOrganizationProfile", data)


    })
}

const getMyOrganizationProfileDashboard = (request, response) => {
    const value = parseInt(request.params.value)
    var sql =
        `SELECT payers_organisation_detail.payer_id as value ,name,first_name AS fullname,
             payers_organisation_detail.logo,payer_end_points.inserted_by,payer_end_point_mapping.endpoint_master_id as endpoint_name,
               users.username,users.password,users.is_admin,
              organisation_type,users.phone as userphone,users.email as useremail, location.state_name,payers_organisation_detail.email, payers_organisation_detail.phone, payers_organisation_detail.address1, payers_organisation_detail.address2, payers_organisation_detail.city,
             postal_code, payer_details, payer_url,users.country,
             payers_organisation_detail.inserted_by, payers_organisation_detail.user_id,payers_organisation_detail.updated_date,organisation_display_name,
             primary_developer_name,primary_developer_title,primary_developer_email,app_url_pre_production,api_url_production FROM payers_organisation_detail
                        left join users on users.user_id =payers_organisation_detail.user_id
                          left join location on location.id =payers_organisation_detail.state
                           left join payer_end_points on payer_end_points.inserted_by =payers_organisation_detail.user_id
						      left join payer_end_point_mapping on payer_end_point_mapping.payer_endpoint_id =payer_end_points.id
                        left join organisation_type on organisation_type.id =payers_organisation_detail.organisation_type_id
                        where payers_organisation_detail.payer_id =${value}`


    console.log(sql, "getMyOrganizationProfileDashboard")
    pool.query(sql, (error, results) => {
        if (error) {
            throw error
        }
        var data = results.rows
        response.status(200).json({ body: 'Success', data })
        console.log("11111111111111", data)


    })
}

const getOrganisationTypes = (request, response) => {

    var sql = `SELECT id as value,organisation_type as label from organisation_type`
    // console.log(sql, "sql")
    pool.query(sql, (error, results) => {
        if (error) {
            throw error
        }
        var data = results.rows
        response.status(200).json({ body: 'Success', data })


    })
}

const getEndpointCount = (request, response) => {
    var sql = `select  p.payer_endpoint_id,payer_end_points.payer_id,payer_end_points.admin_payer_id,
    payers_organisation_detail.is_approve,
        (select  count(*) from payer_end_point_mapping where payer_endpoint_id = p.payer_endpoint_id) point_count
      from payer_end_point_mapping p
       left join payer_end_points ON  payer_end_points.id =p.payer_endpoint_id
        left join  payers_organisation_detail ON  payers_organisation_detail.payer_id =payer_end_points.payer_id`
    console.log(sql, "sql")
    pool.query(sql, (error, results) => {
        if (error) {
            throw error
        }
        var final = results.rows
        const data = Array.from(new Set(final.map(JSON.stringify))).map(JSON.parse);
        response.status(200).json({ body: 'Success', data })
    })
}

const createOrganizationdetails = (req, res) => {

    // console.log(file,"file")
    // if (!file) {
    //    return res.status(400).send({ message: 'Please upload a file.' });
    // }

    const { name, first_name, last_name, email, website, logo, username, type,
        password, address1, address2, city, state, country, zip_code, phone, phone2, fax, ein, policylink, conditionlink } = req.body


    pool.connect(function () {

        bcrypt.hash(password, 10, function (err, hash) {

            var sqlQuery = `INSERT INTO payers_organisation_detail (name,email,website, logo, username,type,fax, ein, policylink,conditionlink,
                        password, address1, address2, city, state, country, zip_code, phone)
                         values ( '${name}','${email}','${website}','${logo}','${username}','${type}','${fax}','${ein}','${policylink}','${conditionlink}',
                     '${hash}', '${address1}', '${address2}', '${city}', '${state}', '${country}', '${zip_code}', '${phone}') RETURNING *`

            console.log(sqlQuery, "sql")
            pool.query(sqlQuery, (error, result, _fields) => {
                if (error) {
                    throw error
                }

                var PayerId = result.rows[0].payer_id;
                console.log(PayerId, "PayerId")

                var sqlQuery2 = `INSERT INTO contact_persons (first_name, last_name,
                        email,phone,payer_id,is_first)  values ('${first_name}','${last_name}',
                        '${email}','${phone2}',${PayerId},1)`
                console.log(sqlQuery2, "contact_persons")
                pool.query(sqlQuery2, (err, result, _fields) => {
                    if (err) throw err;

                });
                var data = result.rows
                res.status(200).json({ body: 'successfully created a user', data })
            });

        })
    })


}

const createAdminOrganizationdetails = (req, res) => {


    const { user_id, name, email, website, logo, username, type,
        password, address1, address2, city, state, country, zip_code, phone, fax, ein, policylink, conditionlink } = req.body


    pool.connect(function () {

        bcrypt.hash(password, 10, function (err, hash) {

            var sqlQuery = `INSERT INTO admin_payers_organisation_detail (user_id,name,email,website, logo, username,type,fax, ein, policylink,conditionlink,
                        password, address1, address2, city, state, country, zip_code, phone)
                         values ('${user_id}','${name}','${email}','${website}','${logo}','${username}','${type}','${fax}','${ein}','${policylink}','${conditionlink}',
                     '${hash}', '${address1}', '${address2}', '${city}', '${state}', '${country}', '${zip_code}', '${phone}') RETURNING *`

            console.log(sqlQuery, "sql")
            pool.query(sqlQuery, (error, result, _fields) => {
                if (error) {
                    throw error
                }
                var data = result.rows
                res.status(200).json({ body: 'successfully created a user', data })
            });

        })
    })


}

const updateImageProfile = (req, res, next) => {
    const { logo } = req.body
    const value = parseInt(req.params.value)

    const file = req.file;
    console.log(file, "file")
    if (!file) {
        return res.status(400).send({ message: 'Please upload a file.' });
    }

    var sql = `UPDATE payers_organisation_detail
    SET  logo= '${file.filename}'
    WHERE payer_id =${value}  RETURNING *
    `
    console.log(sql, "sql")
    pool.query(sql, (error, result) => {

        if (error) {
            throw error
        }
        var data = result.rows
        console.log(data, "data")
        var data = result.rows
        var sqlQuery2 = `SELECT payer_id as value,logo,
        users.username,users.first_name,payers_organisation_detail.user_id FROM payers_organisation_detail
                 left join users on users.user_id =payers_organisation_detail.user_id
             where payers_organisation_detail.payer_id= ${value} `
        console.log(sqlQuery2, sqlQuery2)

        pool.query(sqlQuery2, (err, results) => {
            if (err) throw err;

            var name = results.rows[0].first_name
            var userId = results.rows[0].user_id
            var button_text = 'See Organization Profile'
            var notificationtext = 'Organization profile Icon has been updated by '
            var sqlQuery4 = `INSERT INTO notifications (payer_id, user_id, button_text, redirect_url,notificationtext,name)
        values (${value},'${userId}','${button_text}','${'./MyOrganizationProfile'}','${notificationtext}','${name}')`
            console.log(sqlQuery4, "notifactions")
            pool.query(sqlQuery4, (err, resultss, _fields) => {
                if (err) throw err;

            });
            //  response.status(200).json({ body: 'successfully update a organization', data })
        });
        //   console.log(result.rows[0].email,"email")
        res.status(200).json({ body: 'successfully update a organization', data })

    }
    )


}

//Token Generation API (http://localhost:7000/api/getAzureADToken) -- not used in appication
const getAzureADToken = (request, response) => {

    const endpoint = "https://login.microsoftonline.com/0bd024f1-e61c-461f-b68c-96b715e96cee/oauth2/v2.0/token";
    const requestParams = {
        grant_type: "client_credentials",
        client_id: "bc0d1797-d689-4cfb-a5cc-b94c8e42fd57",
        client_secret: "2-C7Q~BvYmzJeFuxDUdDLfj7gd-BRkL7tyzB~", //client secret value
        scope: "https://graph.microsoft.com/.default"
    };

    requestad.post({ url: endpoint, form: requestParams }, function (err, response2, body) {
        if (err) {
            console.log("error");
        }
        else {
            console.log("Body=" + body);
            let parsedBody = JSON.parse(body);
            if (parsedBody.error_description) {
                console.log("Error=" + parsedBody.error_description);
            }
            else {
                console.log("Access Token=" + parsedBody.access_token);
                let access_token = parsedBody.access_token;
                response.status(200).json({ body: 'successfully token has been generated', access_token })
                // createuserGraphAPI(parsedBody.access_token, userdetails, "");
            }
        }
    });
}

//Getting the Token
function getToken(userdetails, source, userid) {
    console.log("getToken called userdetails =" + userdetails);
    const endpoint = "https://login.microsoftonline.com/0bd024f1-e61c-461f-b68c-96b715e96cee/oauth2/v2.0/token";
    const requestParams = {
        grant_type: "client_credentials",
        client_id: "bc0d1797-d689-4cfb-a5cc-b94c8e42fd57",
        client_secret: "eu78Q~5Topf3hurfDOpck5YOktDVOUA3ccx4zdaF", //client secret value
        scope: "https://graph.microsoft.com/.default"
    };

    requestad.post({ url: endpoint, form: requestParams }, function (err, response, body) {
        if (err) {
            console.log("error");
        }
        else {
            console.log("getToken Body=" + body);
            let parsedBody = JSON.parse(body);
            if (parsedBody.error_description) {
                console.log("getToken Error=" + parsedBody.error_description);
            }
            else {
                console.log("Access Token=" + parsedBody.access_token);
                if (source == 1) {
                    createuserGraphAPI(parsedBody.access_token, userdetails, userid);
                }
                else if (source == 2) {
                    UpdateADUser(parsedBody.access_token);
                    // testGraphAPI(parsedBody.access_token);
                }


            }
        }
    });
}

//Create user in AD useing the token, user details and user id
function createuserGraphAPI(accessToken, userdetails, userid) {
    console.log("testGraphAPI called =" + accessToken);

    const endpoint = "https://graph.microsoft.com/v1.0/users";

    console.log("userdetails====", userdetails);

    requestad.post({
        url: endpoint,
        body: JSON.stringify(userdetails),
        headers: {
            "Authorization": "Bearer " + accessToken,
            "Content-Type": "application/json"
        }
    }, function (err, response, body) {
        if (err) {
            console.log("createuserGraphAPI error====", err);
        }
        else {
            console.log("createuserGraphAPI response=----- " + JSON.stringify(response.body));

            let parsedBody = JSON.parse(body);
            console.log("createuserGraphAPI response ID=----- " + parsedBody.id);
            var sql = `UPDATE users SET  object_id ='${parsedBody.id}'  where user_id =${userid}  RETURNING *`



            console.log(sql, "updateIsApprove")
            pool.query(sql, (error, result) => {
                if (error) {
                    throw error
                }
                console.log("Success createuserGraphAPI=", userid);

            });
            // console.log("bodyerror=----- " + parsedBody.error);
            // if (parsedBody.error_description) {
            //     console.log("Error=" + parsedBody.error_description);
            // }
            // else {
            //     console.log("Access Token=" + parsedBody.access_token);
            //     createuserGraphAPI(parsedBody.access_token);
            // }
        }
    });

}

// Update AD user password (its for testing,this is implemented in user.js)
function UpdateADUser(accessToken) {
    console.log("UpdateADUser called =" + accessToken);
    const userdetails = {
        "passwordProfile": {
            "password": "Amar@123",
            "forceChangePasswordNextSignIn": false
        }
    };
    requestad.patch({
        url: "https://graph.microsoft.com/v1.0/users/69129e4a-1d04-48f4-97f8-5c62d79d1f61",
        body: JSON.stringify(userdetails),
        headers: {
            "Authorization": "Bearer " + accessToken,
            "Content-Type": "application/json"
        }
    }, function (err, response, body) {
        console.log("UpdateADUser body======== ", body);
        console.log("UpdateADUser err======== ", err);
        console.log("UpdateADUser response======== ", response);
    });
}

//get related user details using object id (Its for testing)
function testGraphAPI(accessToken) {
    console.log("testGraphAPI called =" + accessToken);
    requestad.get({
        url: "https://graph.microsoft.com/v1.0/users/69129e4a-1d04-48f4-97f8-5c62d79d1f61",
        // search:
        // {
        //     "displayName":"Vinay C"
        // },
        headers: {
            "Authorization": "Bearer " + accessToken
        }
    }, function (err, response, body) {
        console.log("body======== ", body);
        console.log("err======== ", err);
        console.log("response======== ", response);
    });
}

const updateIsApprove = (request, response) => {
    const value = parseInt(request.params.value)
    var sql = `UPDATE payers_organisation_detail SET  is_approve = 1  where payer_id =${value}  RETURNING *`
    console.log(sql, "updateIsApprove")
    pool.query(sql, (error, result) => {
        if (error) {
            throw error
        }
        var email = result.rows[0].email
        var name = result.rows[0].name
        var mailOptions = {
            from: {
                email: 'developer@health-chain.io',
                name: 'HealthChain'
            },
            to: email,
            subject: 'Approve - Healthchain.io',
            text: `Hello ${name}`,

        };
        sgMail.send(mailOptions, function (error, info) {
            if (error) {
                console.log("not send email")
                return 1
            } else {
                console.log("send email success")
                return 0
            }
        });
        var data = result.rows
        response.status(200).json({ body: 'successfully update a organization', data })

    }
    )
}

const updateIsRejected = (request, response) => {
    const value = parseInt(request.params.value)
    var sql = `UPDATE payers_organisation_detail
    SET is_approve=-1
    WHERE payer_id =${value}  RETURNING *
    `
    console.log(sql, "sql")
    pool.query(sql, (error, result) => {
        if (error) {
            throw error
        }
        var email = result.rows[0].email
        var name = result.rows[0].name
        var mailOptions = {
            from: {
                email: 'developer@health-chain.io',
                name: 'HealthChain'
            },
            to: email,
            subject: 'Rejected - Healthchain.io',
            text: `Hello ${name}`,

        };
        sgMail.send(mailOptions, function (error, info) {
            if (error) {
                console.log("not send email")
                return 1
            } else {
                console.log("send email success")
                return 0
            }
        });
        var data = result.rows
        //  console.log(data,"data")
        //   console.log(result.rows[0].email,"email")
        response.status(200).json({ body: 'successfully update a organization', data })

    }
    )
}

const updateAdminPayerId = (request, response) => {
    const value = parseInt(request.params.value)
    var admin_payer_id = request.body.admin_payer_id

    console.log(request.body, "admin_id")
    var sql = `UPDATE payers_organisation_detail
    SET  admin_payer_id= ${admin_payer_id},is_approve =1
    WHERE payer_id =${value}  RETURNING *
    `
    console.log(sql, "sql")
    pool.query(sql, (error, result) => {

        if (error) {
            throw error
        }

        var sqlQuery2 = `SELECT users.username,users.user_id,users.email,users.passorg FROM payers_organisation_detail
        left join users on users.user_id =payers_organisation_detail.user_id
    where payers_organisation_detail.payer_id= ${value} `
        console.log(sqlQuery2, sqlQuery2)

        pool.query(sqlQuery2, (err, results) => {
            if (err) throw err;

            var password = results.rows[0].passorg;
            var userid = results.rows[0].user_id;

            const cryptr = new Cryptr('ReallySecretKey');
            const decryptedpassword = cryptr.decrypt(password);
            console.log("decryptedpassword", decryptedpassword);

            const requestParams = {
                "displayName": results.rows[0].username,
                "identities": [
                    {
                        "signInType": "emailAddress",
                        "issuer": "healthchainB2c.onmicrosoft.com",
                        "issuerAssignedId": results.rows[0].email
                    }
                ],
                "passwordProfile": {
                    "password": decryptedpassword,
                    "forceChangePasswordNextSignIn": false
                },
                "passwordPolicies": "DisablePasswordExpiration"
            };


            getToken(requestParams, 1, userid);

        });
        var data = result.rows
        //  console.log(data,"data")
        //   console.log(result.rows[0].email,"email")
        response.status(200).json({ body: 'successfully update a organization', data })

    }
    )
}

const updateMyOrgProfile = (req, response) => {
    const { user_id, name, email, website, logo, username, type, organisation_type_id,
        password, address1, address2, city, label, country, zip_code, phone, fax, ein, policylink, conditionlink, organisation_display_name,
        primary_developer_name, primary_developer_title, primary_developer_email, app_url_pre_production, api_url_production,
       } = req.body


    const value = parseInt(req.params.value)

    var sql = `UPDATE payers_organisation_detail
    SET  name ='${name}', address1 ='${address1}',organisation_type_id='${organisation_type_id}', address2 ='${address2}',city ='${city}',state ='${label}',country ='${country}',
    zip_code =${zip_code},phone =${phone},policylink ='${policylink}',website ='${website}',conditionlink='${conditionlink}'
  
    WHERE payer_id =${value}  RETURNING *
    `
    console.log(sql, "UPDATE payers_organisation_detail")
    pool.query(sql, (error, result) => {
        if (error) {
            throw error
        }
        var data = result.rows
        userId = result.rows[0].user_id

        var sqlQuery2 = `SELECT payer_id as value,logo,
        users.username,users.first_name,payers_organisation_detail.user_id FROM payers_organisation_detail
                 left join users on users.user_id =payers_organisation_detail.user_id
             where payers_organisation_detail.payer_id= ${value} `
        console.log(sqlQuery2, sqlQuery2)

        pool.query(sqlQuery2, (err, results) => {
            if (err) throw err;

            var name = results.rows[0].first_name
            var button_text = 'See Organization Profile'
            var notificationtext = 'Organization profile has been updated by'
            var sqlQuery4 = `INSERT INTO notifications (payer_id, user_id, button_text, redirect_url,notificationtext,name)
        values (${value},'${userId}','${button_text}','${'./MyOrganizationProfile'}','${notificationtext}','${name}')`
            console.log(sqlQuery4, "notifactions")
            pool.query(sqlQuery4, (err, resultss, _fields) => {
                if (err) throw err;

            });
            response.status(200).json({ body: 'successfully update a organization', data })
        });

    })
}

const insertdatatopayerhub = async (req, res) => {
    var PayerData = req.body.PayerData
    try {
        for (var i = 0; i < PayerData.length; i++) {
            var sql = `select * FROM admin_payers_organisation_detail where omega_payer_id= ${PayerData[i].payer_id}`
            let getAllpayers = await pool.query(sql)
            console.log(getAllpayers.rows, "getAllpayers")
            var result = getAllpayers.rows
            if (result[0]) {
                // var omega_payer_id = result[0].omega_payer_id
                console.log("omega_payer_id already exits", PayerData[i])
                var sqlQuery1 = `UPDATE public.admin_payers_organisation_detail
                SET  name='${PayerData[i].name}', email='${PayerData[i].email}', phone='${PayerData[i].phone}', address1='${PayerData[i].address1}',
                address2='${PayerData[i].address2}', city='${PayerData[i].city}', country='${PayerData[i].country}', website='${PayerData[i].website}',
                logo='${PayerData[i].logo}', fax='${PayerData[i].fax}', policylink='${PayerData[i].policylink}',
                conditionlink='${PayerData[i].conditionlink}',
                state='${PayerData[i].state}', zip_code='${PayerData[i].zip_code}'
                WHERE omega_payer_id =${PayerData[i].payer_id}  RETURNING *;`
                console.log(sqlQuery1, " update sqlQuery")
                pool.query(sqlQuery1, (error, data2) => {
                    if (error) {
                        throw error
                    }
                    var finalData = data2.rows
                    console.log(finalData, "finalData")
                })
            }
            else {
                var sqlQuery4 = `INSERT INTO admin_payers_organisation_detail(omega_payer_id,name,email,phone,address1,address2,
                city,zip_code,country,website,logo,fax,ein,policylink,conditionlink,type,state,base_url,scope)
                VALUES ('${PayerData[i].payer_id}','${PayerData[i].name}', '${PayerData[i].email}', '${PayerData[i].phone}', '${PayerData[i].address1}',
                '${PayerData[i].address2}', '${PayerData[i].city}', '${PayerData[i].zip_code}', '${PayerData[i].country}', 
                '${PayerData[i].website}','${PayerData[i].logo}','${PayerData[i].fax}','${PayerData[i].ein}','${PayerData[i].policylink}',
                '${PayerData[i].conditionlink}','${PayerData[i].type}','${PayerData[i].state}','${PayerData[i].base_url}','${PayerData[i].scope}') RETURNING *;`
                console.log(sqlQuery4, "insert sqlQuery4")
                pool.query(sqlQuery4, (error, data2) => {
                    if (error) {
                        throw error
                    }
                    var finalData = data2.rows
                    console.log(finalData, "finalData")
                    var admin_payer_id = data2.rows[0].admin_payer_id
                    var base_url = data2.rows[0].base_url
                    var sqlQuery3 = `INSERT INTO payer_end_points(admin_payer_id,base_url)
                                    VALUES ('${admin_payer_id}','${base_url}') RETURNING * `
                    pool.query(sqlQuery3, (error, data) => {
                        if (error) {
                            throw error
                        }
                        var payer_endpoint_id = data.rows[0].id
                        console.log(payer_endpoint_id, "payer_end_points")
                        var scope = data2.rows[0].scope
                        console.log(scope, "scope")
                        var endpointarray
                        if (scope.indexOf(',') != -1) {
                            endpointarray = scope.split(',');
                        }
                        else {
                            endpointarray = scope.split(' ')
                        }
                        console.log(endpointarray, 'endpointarray');
                        var ArrayScope = ["openid", "profile", "fhirUser", "offline_access"]
                        for (var i = 0; i < endpointarray.length; i++) {
                            console.log(endpointarray[i], 'endpointarray[i]');
                            if (!ArrayScope.includes(endpointarray[i])) {
                                var sqlQuery2 = `INSERT INTO end_point_master(admin_payer_id,endpoint_name)
                                 VALUES ('${admin_payer_id}','${endpointarray[i]}') RETURNING * `
                                // console.log(sqlQuery4, "sqlQuery4")
                                pool.query(sqlQuery2, (error, data3) => {
                                    if (error) {
                                        throw error
                                    }
                                    var end_point_master_data = data3.rows
                                    console.log(end_point_master_data, "end_point_master_data")
                                    endpoint_master = data3.rows[0].id
                                    console.log(endpoint_master, "endpoint_master")
                                    console.log(payer_endpoint_id, "payer_endpoint_id")
                                    var sqlQuery4 = `INSERT INTO payer_end_point_mapping(payer_endpoint_id,endpoint_master_id)
                                    VALUES ('${payer_endpoint_id}','${endpoint_master}') RETURNING * `
                                    console.log(sqlQuery4, "sqlQuery4")
                                    pool.query(sqlQuery4, (error, data4) => {
                                        if (error) {
                                            throw error
                                        }
                                        endpoint_mapping = data4.rows
                                        console.log(endpoint_mapping, "endpoint_mapping")
                                    })
                                })
                            }
                        }
                    })
                })
            }
        }
        res.status(200).json({ body: 'successfully inserted on db' })
    }
    catch (e) {
        console.log(e, "error");
    }
}

const HomeOrganisationDirectory = (request, response) => {
    const name = request.params.payername;
    const label = request.params.location;
    const organizationtype = request.params.organizationtype;
    // const value = parseInt(request.params.payerid)
    console.log(name, "name")
    console.log(organizationtype, "organizationtype")

    var labelArr = label.split(',');
    console.log(labelArr, "labelArr")
    const labelFinal = labelArr.map(c => `'${c}'`).join(', ');
    var organizationtypeArr = organizationtype.split(',');
    console.log(organizationtypeArr, "organizationtypeArr")
    const organizationtypeFinal = organizationtypeArr.map(c => `'${c}'`).join(', ');



    console.log(labelFinal, organizationtypeFinal, "labelFinal")

    var sqlquery = `SELECT distinct payer_id as value ,state as label,name,location.state_name,
    organisation_type as type, email, phone, address1, address2, city,
   postal_code, website, end_point, inserted_by,
   user_id,updated_date,policylink as policylink,conditionlink as conditionlink, 'payer' as payertype FROM payers_organisation_detail
   left join organisation_type on organisation_type.id = payers_organisation_detail.organisation_type_id
   left join location on location.id =payers_organisation_detail.state`

    var sqlquery2 = `UNION
   SELECT distinct admin_payer_id as value ,state as label,name,location.state_name,
    organisation_type as type, email, phone, address1, address2, city,
   zip_code, website,scope,  inserted_by,
   inserted_by,updated_date,policylink as policylink,conditionlink as conditionlink, 'adminPayer' as payertype FROM admin_payers_organisation_detail
   left join organisation_type on organisation_type.id =admin_payers_organisation_detail.type
   left join location on location.id =admin_payers_organisation_detail.state`

    if (name == 'All' && label == 'All' && organizationtype == 'All') {
        sqlquery = sqlquery + ' ' + sqlquery2;
        console.log('sql query ppppp =1 ', sqlquery);
    }
    else if (name != 'All' && label == 'All' && organizationtype == 'All') {
        sqlquery = sqlquery + ` where  Lower(name) like Lower('${name}%')  `
        sqlquery2 = sqlquery2 + ` where  Lower(name) like Lower('${name}%')  `
        sqlquery = sqlquery + ' ' + sqlquery2;
        // console.log('sql query ppppp = 2', sqlquery);
    }
    else if (name == 'All' && label != 'All' && organizationtype == 'All') {
        sqlquery = sqlquery + ` where state in(${labelFinal})` + ' ' + sqlquery2 + ` and state in(${labelFinal})`
        console.log('sql query ppppp = 3', sqlquery);
    }
    else if (name == 'All' && label == 'All' && organizationtype != 'All') {
        sqlquery = sqlquery + ` where organisation_type_id in(${organizationtypeFinal})`
        sqlquery2 = sqlquery2 + ` where type in(${organizationtypeFinal})`
        sqlquery = sqlquery + ' ' + sqlquery2;
        // console.log('sql query ppppp = 4', sqlquery);
    }
    else if (name != 'All' && label != 'All' && organizationtype == 'All') {
        sqlquery = sqlquery + ` where state in(${labelFinal}) and  name like '${name}%'`
        sqlquery2 = sqlquery2 + ` where state in(${labelFinal}) and  name like '${name}%'`
        sqlquery = sqlquery + ' ' + sqlquery2;
        // console.log('sql query ppppp = 5', sqlquery);
    }
    else if (name == 'All' && label != 'All' && organizationtype != 'All') {
        sqlquery = sqlquery + ` where state in(${labelFinal}) and organisation_type_id  in(${organizationtypeFinal})`
        sqlquery2 = sqlquery2 + ` where state in(${labelFinal}) and type  in(${organizationtypeFinal})`
        sqlquery = sqlquery + ' ' + sqlquery2;
        // console.log('sql query ppppp = 6', sqlquery);
    }
    else {
        sqlquery = sqlquery + ` where state in(${labelFinal}) and  name like '${name}%'  and organisation_type_id  in(${organizationtypeFinal})`
        sqlquery2 = sqlquery2 + ` where state in(${labelFinal}) and  name like '${name}%'  and type  in(${organizationtypeFinal})`
        sqlquery = sqlquery + ' ' + sqlquery2;
        // console.log('sql query ppppp = 7', sqlquery);
    }

    console.log('sql query ppppp = All', sqlquery);


    pool.query(sqlquery, function (err, results) {

        if (err) {
            response.status(500).send("Error in execute query");
        }




        else {
            var data = results.rows

            console.log(data, "data")



            response.status(200).json({ body: 'Success', data })
        }
    })
}

const getAllOmegaPayers = (req, res) => {
    var sql = `SELECT omega_payer_id as payer_id, name, email,organisation_type.organisation_type as type,
    phone, address1, address2, city,location.state_name as state,
    zip_code, country, website, logo, fax, policylink, conditionlink,
    base_url, scope,admin_payers_organisation_detail.updated_date,
    admin_payers_organisation_detail.inserted_date
    FROM public.admin_payers_organisation_detail
    left join location on location.id = admin_payers_organisation_detail.state
    left join organisation_type on organisation_type.id = admin_payers_organisation_detail.type`

    pool.query(sql, (error, results) => {
        if (error) {
            throw error
        }
        var PayerData = results.rows
        console.log(PayerData,"admin_payers_organisation_detail")
        res.status(200).json({ body: 'Success', PayerData })
    })
   

}


module.exports = {
    getAllOmegaPayers,
    HomeOrganisationDirectory,
    insertdatatopayerhub,
    updateAdminPayerId,
    updateIsApprove,
    updateIsRejected,
    getOrganisationById,
    createOrganisation,
    updateOrganisation,
    deleteOrganisation,
    getOrganisationByUserId,
    Organisation,
    getBothPayers,
    getOrganisationId,
    getOrganisationAddMyList,
    createOrganizationdetails,
    getMyOrganizationProfile,
    getOrganisationTypes,
    createAdminOrganizationdetails,
    getStates,
    getAllAdminOrganisation,
    getEndpointCount,
    getAllOrganisation,
    getAdminOrganisationId,
    updateMyOrgProfile,
    updateImageProfile,
    getAzureADToken,
    getMyOrganizationProfileDashboard,
    getPayersEnvironment


}


// const insertdatatopayerhub = (req, res) => {
//     var PayerData = req.body.PayerData
//     for (var i = 0; i < PayerData.length; i++) {
//         var sqlQuery4 = `INSERT INTO admin_payers_organisation_detail(omega_payer_id,name,email,phone,address1,address2,city,zip_code,country,
//             website,logo,fax,ein,policylink,conditionlink,type,state,base_url,scope)
//          VALUES ('${PayerData[i].payer_id}','${PayerData[i].name}', '${PayerData[i].email}', '${PayerData[i].phone}', '${PayerData[i].address1}',
//           '${PayerData[i].address2}', '${PayerData[i].city}', '${PayerData[i].zip_code}', '${PayerData[i].country}', 
//           '${PayerData[i].website}','${PayerData[i].logo}','${PayerData[i].fax}','${PayerData[i].ein}','${PayerData[i].policylink}',
//           '${PayerData[i].conditionlink}','${PayerData[i].type}','${PayerData[i].state}','${PayerData[i].base_url}','${PayerData[i].scope}') RETURNING *;`
//         console.log(sqlQuery4, "sqlQuery4")
//         pool.query(sqlQuery4, (error, data2) => {
//             if (error) {
//                 throw error
//             }
//             var finalData = data2.rows
//             var admin_payer_id = data2.rows[0].admin_payer_id
//             var base_url = data2.rows[0].base_url
//             console.log(finalData, "finalData")
//             var sqlQuery3 = `INSERT INTO payer_end_points(admin_payer_id,base_url)
//                                          VALUES ('${admin_payer_id}','${base_url}') RETURNING * `

//             pool.query(sqlQuery3, (error, data) => {
//                 if (error) {
//                     throw error
//                 }
//                 var payer_endpoint_id = data.rows[0].id
//                 console.log(payer_endpoint_id, "payer_end_points")
//                 var scope = data2.rows[0].scope
//                 console.log(scope, "scope")
//                 var endpointarray

//                 if (scope.indexOf(',') != -1) {

//                     endpointarray = scope.split(',');

//                 }

//                 else {
//                     endpointarray = scope.split(' ')

//                 }
//                 console.log(endpointarray, 'endpointarray');

//                 var ArrayScope = ["openid", "profile", "fhirUser", "offline_access"]

//                 for (var i = 0; i < endpointarray.length; i++) {
//                     console.log(endpointarray[i], 'endpointarray[i]');

//                     // !ArrayScope.includes(endpointArray[i])
//                     if (!ArrayScope.includes(endpointarray[i])) {
//                         var sqlQuery2 = `INSERT INTO end_point_master(admin_payer_id,endpoint_name)
//                         VALUES ('${admin_payer_id}','${endpointarray[i]}') RETURNING * `
//                         // console.log(sqlQuery4, "sqlQuery4")
//                         pool.query(sqlQuery2, (error, data3) => {
//                             if (error) {
//                                 throw error
//                             }
//                             var end_point_master_data = data3.rows
//                             console.log(end_point_master_data, "end_point_master_data")
//                             endpoint_master = data3.rows[0].id
//                             console.log(endpoint_master, "endpoint_master")
//                             console.log(payer_endpoint_id, "payer_endpoint_id")

//                             var sqlQuery4 = `INSERT INTO payer_end_point_mapping(payer_endpoint_id,endpoint_master_id)
//                     VALUES ('${payer_endpoint_id}','${endpoint_master}') RETURNING * `

//                             console.log(sqlQuery4, "sqlQuery4")

//                             pool.query(sqlQuery4, (error, data4) => {
//                                 if (error) {
//                                     throw error
//                                 }
//                                 endpoint_mapping = data4.rows
//                                 console.log(endpoint_mapping, "endpoint_mapping")
//                             })
//                         })
//                     }
//                 }
//             })
//         })
//     }
//     res.status(200).json({ body: 'successfully inserted on db' })
// }
// const readCsvtoInsertdb = (request, response) => {
//     let stream = fs.createReadStream("PayerData.csv");
//     let csvData = [];
//     let csvStream = fastcsv
//         .parse()
//         .on("data", function (data) {
//             csvData.push(data);
//         })
//         .on("end", function () {
//             csvData.shift();
//             const query =
//                 "INSERT INTO admin_payers_organisation_detail (user_id,name,email,username,password,phone,address1,address2,city,zip_code,country,website,logo,fax,ein,policylink,conditionlink,inserted_date,updated_date,type,state,base_url,scope) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10,$11, $12, $13, $14, $15, $16, $17,$18,$19,$20,$21,$22,$23) RETURNING *";
           
//             console.log(query, "query")
//             pool.connect((err, client, done) => {
//                 if (err) throw err;
//                 try {
//                     csvData.forEach(row => {

//                         client.query(query, row, (err, res) => {

//                             if (err) {
//                                 console.log(err.stack);

//                             } else {
//                                 // console.log(csvData, "csvData")
//                                 console.log("inserted " + res.rowCount + " row:", row[21]);
//                                 console.log("admin_payer_id " + res.rows[0].admin_payer_id);
//                                 console.log("count ", csvData.length);
//                                 var base_url = row[21]
//                                 var admin_payer_id = res.rows[0].admin_payer_id
//                                 console.log(base_url, "base_url")
//                                 console.log(admin_payer_id, "admin_payer_id")

//                                 var sqlQuery3 = `INSERT INTO payer_end_points(admin_payer_id,base_url)
//                                                 VALUES ('${admin_payer_id}','${base_url}') RETURNING * `

//                                 pool.query(sqlQuery3, (error, data) => {
//                                     if (error) {
//                                         throw error
//                                     }
//                                     console.log(data.rows, "payer_end_points")
//                                     var scope = row[22]
//                                     console.log(scope, "scope")
//                                     var payer_endpoint_id = data.rows[0].id
//                                     var endpointarray = scope.split('/')
//                                     //console.log(endpointarray, 'endpointarray');

//                                     for (var i = 0; i < endpointarray.length; i++) {
//                                        // console.log(endpointarray[i], 'endpointarray[i]');
//                                         var endpointarraySpace = endpointarray[i].split(' ')
//                                          console.log(endpointarraySpace[0], 'endpointarraySpace');

//                                         var sqlQuery4 = `INSERT INTO end_point_master(admin_payer_id,endpoint_name)
//                                                         VALUES ('${admin_payer_id}','${endpointarraySpace[0]}') RETURNING * `
//                                         //  console.log(sqlQuery4, "sqlQuery4")
//                                         pool.query(sqlQuery4, (error, data2) => {
//                                             if (error) {
//                                                 throw error
//                                             }
//                                             var finalData = data2.rows
//                                             console.log(finalData, "finalData")
//                                             endpoint_master = data2.rows[0].id
//                                             console.log(endpoint_master, "endpoint_master")
//                                             console.log(payer_endpoint_id, "payer_endpoint_id")

//                                             var sqlQuery4 = `INSERT INTO payer_end_point_mapping(payer_endpoint_id,endpoint_master_id)
//                                                     VALUES ('${payer_endpoint_id}','${endpoint_master}') RETURNING * `

//                                             console.log(sqlQuery4, "sqlQuery4")

//                                             pool.query(sqlQuery4, (error, data3) => {
//                                                 if (error) {
//                                                     throw error
//                                                 }
//                                                 endpoint_mapping = data3.rows
//                                                 console.log(endpoint_mapping, "endpoint_mapping")

//                                             })

//                                         })
//                                     }

//                                 })
//                             }

//                         });
//                     });
//                 }

//                 finally {
//                     done();
//                     response.status(200).json({ body: 'successfully inserted on db' })
//                 }
//             });
//         });

//     stream.pipe(csvStream);
// }











