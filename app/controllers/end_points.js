var pool = require('../config/db.config')

const getEndPoints = (request, response) => {
    const payer_id = parseInt(request.params.value)
    var sql = `SELECT payer_end_points.id,end_point_master.endpoint_name,payer_end_points.other_endpoint_name, payer_end_points.base_url, payer_end_points.auth_scope,
admin_payers_organisation_detail.name,admin_payers_organisation_detail.admin_payer_id
FROM payer_end_points
LEFT JOIN admin_payers_organisation_detail ON payer_end_points.payer_id = admin_payers_organisation_detail.admin_payer_id
LEFT JOIN payer_end_point_mapping ON payer_end_point_mapping.payer_endpoint_id = payer_end_points.id
LEFT JOIN end_point_master ON end_point_master.id = payer_end_point_mapping.endpoint_master_id
where  payer_end_points.admin_payer_id =${payer_id}`
    pool.query(sql, (error, results) => {
        if (error) {
            throw error
        }
        var data = results.rows
        response.status(200).json({ body: 'Success', data })
        console.log(data)

    })
}
const getAllEndPoints = (request, response) => {
    const value = parseInt(request.params.value)
    var sql = `SELECT payer_end_points.id,end_point_master.endpoint_name,payer_end_points.other_endpoint_name, payer_end_points.base_url, payer_end_points.auth_scope,
    payer_end_points.auth_type, payer_end_points.token_url,payer_end_points.return_url, payer_end_points.authorize_url,payer_end_points.updated_date,
payers_organisation_detail.name,payers_organisation_detail.payer_id
FROM payer_end_points
LEFT JOIN payers_organisation_detail ON payer_end_points.payer_id = payers_organisation_detail.payer_id
LEFT JOIN payer_end_point_mapping ON payer_end_point_mapping.payer_endpoint_id = payer_end_points.id
LEFT JOIN end_point_master ON end_point_master.id = payer_end_point_mapping.endpoint_master_id
LEFT JOIN users ON users.user_id = payers_organisation_detail.user_id
where  payers_organisation_detail.payer_id =${value}`
    pool.query(sql, (error, results) => {
        if (error) {
            throw error
        }
        var data2 = results.rows
        var data = data2.filter(function (e) { return e.endpoint_name != '' && e.endpoint_name != null });
        console.log(data, "getAllEndPoints");
        response.status(200).json({ body: 'Success', data })

    })
}
const getEndPointsAll = (request, response) => {
    const user_id = parseInt(request.params.user_id)
     //const payer_environment_type = request.params.payer_environment_type
    var sql = `SELECT payer_end_point_mapping.id,payer_end_points.id as payer_endpoint_id,end_point_master.endpoint_name,
	end_point_master.id as end_point_master_id,
    payer_end_points.base_url,payer_end_points.other_endpoint_name, payer_end_points.updated_date, payer_end_points.auth_scope,
    payer_end_points.return_url,payer_end_points.token_url, payer_end_points.auth_type, payer_end_points.authorize_url,payer_end_points.payer_environment_type,
    payers_organisation_detail.name,payers_organisation_detail.payer_id FROM payer_end_points
    LEFT JOIN payers_organisation_detail ON payer_end_points.payer_id = payers_organisation_detail.payer_id
    LEFT JOIN payer_end_point_mapping ON payer_end_point_mapping.payer_endpoint_id = payer_end_points.id
    LEFT JOIN end_point_master ON end_point_master.id = payer_end_point_mapping.endpoint_master_id
    LEFT JOIN users ON users.user_id = payers_organisation_detail.user_id
    where  payers_organisation_detail.user_id =${user_id}`
//and payer_end_points.payer_environment_type ='${payer_environment_type}'
    console.log(sql,"sql")

    pool.query(sql, (error, results) => {
        if (error) {
            throw error
        }

        var data2 = results.rows
        var data = data2.filter(function (e) { return e.endpoint_name != '' && e.endpoint_name != null });


        //   console.log(data, "getEndPointsAllByUserId");

        response.status(200).json({ body: 'Success', data })

    })
}
const getEndPointsUrls = (request, response) => {
    const payer_id = parseInt(request.params.payer_id)
    var sql = `select * from payer_end_points where payer_id =${payer_id}`
    pool.query(sql, (error, results) => {
        if (error) {
            throw error
        }
        var data = results.rows
        response.status(200).json({ body: 'Success', data })
        console.log(data)

    })
}
const getTypeEndPoints = (request, response) => {
    const user_id = parseInt(request.params.user_id)
     const payer_environment_type = request.params.payer_environment_type
    var sql = `SELECT payer_end_point_mapping.id,payer_end_points.id as payer_endpoint_id,end_point_master.endpoint_name,
	end_point_master.id as end_point_master_id,
    payer_end_points.base_url,payer_end_points.other_endpoint_name, payer_end_points.updated_date, payer_end_points.auth_scope,
    payer_end_points.return_url,payer_end_points.token_url, payer_end_points.auth_type, payer_end_points.authorize_url,payer_end_points.payer_environment_type,
    payers_organisation_detail.name,payers_organisation_detail.payer_id FROM payer_end_points
    LEFT JOIN payers_organisation_detail ON payer_end_points.payer_id = payers_organisation_detail.payer_id
    LEFT JOIN payer_end_point_mapping ON payer_end_point_mapping.payer_endpoint_id = payer_end_points.id
    LEFT JOIN end_point_master ON end_point_master.id = payer_end_point_mapping.endpoint_master_id
    LEFT JOIN users ON users.user_id = payers_organisation_detail.user_id
    where  payers_organisation_detail.user_id =${user_id}
    and payer_end_points.payer_environment_type ='${payer_environment_type}'`
    console.log(sql,"sql")

    pool.query(sql, (error, results) => {
        if (error) {
            throw error
        }

        var data2 = results.rows
        var data = data2.filter(function (e) { return e.endpoint_name != '' && e.endpoint_name != null });


        //   console.log(data, "getEndPointsAllByUserId");

        response.status(200).json({ body: 'Success', data })

    })
}
const getAdminEndPointsAll = (request, response) => {
    const user_id = parseInt(request.params.user_id)
    var sql = `select payer_end_points.id,end_point_master.endpoint_name, payer_end_points.base_url, payer_end_points.auth_scope
from public.payer_end_points
LEFT JOIN payer_end_point_mapping ON payer_end_point_mapping.payer_endpoint_id = payer_end_points.id
LEFT JOIN end_point_master ON end_point_master.id = payer_end_point_mapping.endpoint_master_id
where inserted_by =${user_id}`
    pool.query(sql, (error, results) => {
        if (error) {
            throw error
        }
        var data = results.rows
        response.status(200).json({ body: 'Success', data })
        console.log(data)

    })
}
const createEndPoints = (request, response) => {
    const { payer_id, endpoint_name, base_url, auth_scope, user_id, name, other_endpoint_name, token_url,auth_type,
        authorize_url,return_url,payer_environment_type} = request.body
    var endpointarray = other_endpoint_name.split(',');

    console.log(endpointarray, 'endpointarray');
    var newendpoints = [];
    var EndpointName = endpoint_name
    console.log(EndpointName, "EndpointName")
    for (var i = 0; i < endpointarray.length; i++) {
        console.log(endpointarray[i], 'endpointarray[i]');

        var sqlQuery4 = `INSERT INTO end_point_master(payer_id,endpoint_name)
                    VALUES (${payer_id},'${endpointarray[i]}') RETURNING * `
        console.log(sqlQuery4, "sqlQuery4")
        pool.query(sqlQuery4, (error, results) => {
            if (error) {
                throw error
            }
            newendpoints.push(results.rows[0].id);
            console.log("Loop Started ", i)
            console.log(" Started minus value-- ", endpointarray.length)
            if (i == endpointarray.length) {
                console.log("Loop ended======== ", newendpoints)
            }
        })
    }

    const sleep = (waitTimeInMs) => new Promise(resolve => setTimeout(resolve, waitTimeInMs));
    

    sleep(1000).then(() => {
        var sql = `INSERT INTO payer_end_points(payer_id,endpoint_name, base_url, auth_scope,inserted_by,
            token_url,auth_type,authorize_url,return_url,payer_environment_type)
        VALUES ('${payer_id}','${endpoint_name}','${base_url}','${auth_scope}','${user_id}',
        '${token_url}','${auth_type}','${authorize_url}','${return_url}','${payer_environment_type}') RETURNING * `
        console.log(sql, "sql")
        pool.query(sql, (error, results) => {
            if (error) {
                throw error
            }
            var payer_endpoint_id = results.rows[0].id;
            console.log(payer_endpoint_id, "payer_end_points")

            var sqlQuery2 = `select id as endpoint_master_id, endpoint_name from end_point_master`

            pool.query(sqlQuery2, (err, result, _fields) => {
                if (err) throw err;
                var data = result.rows
                console.log(data, "end_point_master")

                for (var i = 0; i < endpoint_name.length; i++) {
                    console.log(endpoint_name, "endpoint_name")

                    console.log(endpoint_name[i], "end_point_master")
                    var sqlQuery3 = `INSERT INTO payer_end_point_mapping (payer_endpoint_id,endpoint_master_id)
        values ('${payer_endpoint_id}','${endpoint_name[i]}')`

                    console.log("payer_end_point_mapping", sqlQuery3)
                    pool.query(sqlQuery3, (err, resultss, _fields) => {
                        if (err) throw err;

                    });

                }

                console.log(newendpoints.length, "newendpoints length")
                for (var j = 0; j < newendpoints.length; j++) {

                    console.log(newendpoints[j], "newendpoints")
                    var sqlQuery3 = `INSERT INTO payer_end_point_mapping (payer_endpoint_id,endpoint_master_id)
        values ('${payer_endpoint_id}','${newendpoints[j]}')`

                    console.log("newendpoints mapping", sqlQuery3)
                    pool.query(sqlQuery3, (err, resultss, _fields) => {
                        if (err) throw err;

                    });
                }
                var button_text = 'See Endpoint'
                var notificationtext = 'Patient Access API Endpoints are added by administrator'
                var sqlQuery4 = `INSERT INTO notifications (payer_id, user_id, button_text, redirect_url,notificationtext,name)
            values ('${payer_id}','${user_id}','${button_text}','${'./Endpoints'}','${notificationtext}','${name}')`
                console.log(sqlQuery4, "notifactions")
                pool.query(sqlQuery4, (err, resultss, _fields) => {
                    if (err) throw err;

                });
            });

            var data = results.rows
            response.status(200).json({ body: 'successfully created a end points', data })
        })
    });

    //   }


    // })



}

const createAdminEndPoints = (request, response) => {

    const pp = request.body.admin_payer_id

    console.log(pp, "admin_payer_id")

    const { admin_payer_id, endpoint_name, base_url, auth_scope, user_id, other_endpoint_name, value } = request.body
    console.log(other_endpoint_name, 'other_endpoint_name');
    console.log(endpoint_name, 'endpoint_name');
    var endpointarray = other_endpoint_name.split(',');

    console.log(endpointarray, 'endpointarray');
    var newendpoints = [];
    for (var i = 0; i < endpointarray.length; i++) {
        console.log(endpointarray[i], 'endpointarray[i]');

        var sqlQuery4 = `INSERT INTO end_point_master(admin_payer_id,endpoint_name)
                    VALUES ('${admin_payer_id}','${endpointarray[i]}') RETURNING * `
        //  console.log(sqlQuery4, "sqlQuery4")
        pool.query(sqlQuery4, (error, results) => {
            if (error) {
                throw error
            }
            var endpoint_id = results.rows[0].id;
            newendpoints.push(results.rows[0].id);
            console.log(" admin Loop Started ", i)
            console.log(" Started minus value-- ", endpointarray.length)
            if (i == endpointarray.length) {
                console.log("admin Loop ended======== ", newendpoints)


            }


        })
    }
    var sql = `INSERT INTO payer_end_points(admin_payer_id,endpoint_name, base_url, auth_scope,inserted_by)
        VALUES ('${admin_payer_id}','${endpoint_name}','${base_url}','${auth_scope}','${user_id}') RETURNING * `
    console.log(sql, "sql")
    pool.query(sql, (error, results) => {
        if (error) {
            throw error
        }
        console.log("newendpoints ======== ", newendpoints)
        var payer_endpoint_id = results.rows[0].id;
        console.log(payer_endpoint_id, "payer_end_points")

        var sqlQuery2 = `select id as endpoint_master_id, endpoint_name from end_point_master`

        pool.query(sqlQuery2, (err, result, _fields) => {
            if (err) throw err;
            var data = result.rows
            console.log(data, "end_point_master")
            // var payer_end_point = result.rows[0].endpoint_master_id;


            for (var i = 0; i < endpoint_name.length; i++) {
                console.log(endpoint_name, "endpoint_name")

                console.log(endpoint_name[i], "end_point_master")
                var sqlQuery3 = `INSERT INTO payer_end_point_mapping (payer_endpoint_id,endpoint_master_id)
        values ('${payer_endpoint_id}','${endpoint_name[i]}')`

                console.log("payer_end_point_mapping", sqlQuery3)
                pool.query(sqlQuery3, (err, resultss, _fields) => {
                    if (err) throw err;

                });

            }
            console.log(newendpoints.length, "newendpoints length")
            for (var j = 0; j < newendpoints.length; j++) {
                // console.log(newendpoints, "endpoint_name")

                console.log(newendpoints[j], " admin newendpoints")
                var sqlQuery3 = `INSERT INTO payer_end_point_mapping (payer_endpoint_id,endpoint_master_id)
        values ('${payer_endpoint_id}','${newendpoints[j]}')`

                console.log("newendpoints mapping", sqlQuery3)
                pool.query(sqlQuery3, (err, resultss, _fields) => {
                    if (err) throw err;

                });

            }


            //   var EndpointMasterId = result.rows.endpoint_master_id;

        });

        var data = results.rows
        response.status(200).json({ body: 'successfully created a end points', data })
        // console.log(data)

    })
}

const deleteEndPoints = (request, response) => {
    const id = parseInt(request.params.id)
      var sql = `DELETE FROM payer_end_point_mapping WHERE id = ${id}`
    // var sql = `DELETE FROM payer_end_points AS a USING 
    //            payer_end_point_mapping AS b
    //            WHERE 
    //            a.id = b.payer_endpoint_id
    //            AND a.id = ${id} AND b.payer_endpoint_id =${id}`
    console.log(sql, "delete endpoints")
    pool.query(sql, (error, results) => {
        if (error) {
            throw error
        }
        // response.status(200).send(`User deleted with ID: ${id}`)
        response.status(200).json({ body: 'successfully deleted a end points ' })

    })

}
const getEndPointsList = (request, response) => {

    pool.query('select * ,id as end_point_master_id from public.end_point_master', (error, results) => {
        if (error) {
            throw error
        }
        var data = results.rows
        response.status(200).json({ body: 'successfully', data })

    })
}
const getIdByEndPoints = (request, response) => {
    const id = parseInt(request.params.id)
    pool.query('select * FROM payer_end_points WHERE id = $1', [id], (error, results) => {
        if (error) {
            throw error
        }
        // response.status(200).send(`User deleted with ID: ${id}`)
        response.status(200).json({ body: 'successfully deleted a end points ' })

    })
}
const updateEndPoints = (request, response) => {
    var envType = request.params.envType
    var payer_end_points_id = request.params.payer_end_points_id
    const { payer_id, endpoint_name, base_url, auth_scope, user_id, name, other_endpoint_name,
        token_url,auth_type,authorize_url,return_url }= request.body
    console.log(request.body, "request.body")

    console.log(other_endpoint_name, "other_endpoint_name")

    // Array.isArray(variable)

    if (Array.isArray(other_endpoint_name)) {
        console.log(other_endpoint_name, "array")
        var endpointarray = other_endpoint_name;
    }
    else {
        var endpointarray = other_endpoint_name.split(',');
    }

    sql = `delete FROM end_point_master where payer_id=${payer_id}`
    console.log(sql, "delete endpoints")
    pool.query(sql, (error, results) => {
        if (error) {
            throw error
        }
        var data = results.rows
        console.log(data, "data")

    })


    console.log(endpointarray, 'endpointarray');
    var newendpoints = [];

    for (var i = 0; i < endpointarray.length; i++) {
        console.log(endpointarray[i], 'endpointarray[i]');
        var sqlQuery4 = `INSERT INTO end_point_master(payer_id,endpoint_name)
                    VALUES (${payer_id},'${endpointarray[i]}') RETURNING * `
        console.log(sqlQuery4, "sqlQuery4")
        pool.query(sqlQuery4, (error, results) => {
            if (error) {
                throw error
            }
            newendpoints.push(results.rows[0].id);
            console.log("Loop Started ", i)
            console.log(" Started minus value-- ", endpointarray.length)
            if (i == endpointarray.length) {
                console.log("Loop ended======== ", newendpoints)
            }
        })
    }

    const sleep = (waitTimeInMs) => new Promise(resolve => setTimeout(resolve, waitTimeInMs));

    sleep(1000).then(() => {
        // var sql = `INSERT INTO payer_end_points(payer_id,endpoint_name,inserted_by)
        // VALUES ('${payer_id}','${endpoint_name}','${user_id}') RETURNING * `
        // console.log(sql, "sql")
        // pool.query(sql, (error, results) => {
        //     if (error) {
        //         throw error
        //     }
            var payer_endpoint_id = payer_end_points_id
            console.log(payer_endpoint_id, "payer_end_points")

            var sqlQuery2 = `select id as endpoint_master_id, endpoint_name from end_point_master`

            pool.query(sqlQuery2, (err, result, _fields) => {
                if (err) throw err;
                var data = result.rows
                console.log(data, "end_point_master")

                for (var i = 0; i < endpoint_name.length; i++) {
                    console.log(endpoint_name, "endpoint_name")

                    console.log(endpoint_name[i], "end_point_master")
                    var sqlQuery3 = `INSERT INTO payer_end_point_mapping (payer_endpoint_id,endpoint_master_id)
        values ('${payer_endpoint_id}','${endpoint_name[i]}')`

                    console.log("payer_end_point_mapping", sqlQuery3)
                    pool.query(sqlQuery3, (err, resultss, _fields) => {
                        if (err) throw err;

                    });

                }

                console.log(newendpoints.length, "newendpoints length")
                for (var j = 0; j < newendpoints.length; j++) {

                    console.log(newendpoints[j], "newendpoints")
                    var sqlQuery3 = `INSERT INTO payer_end_point_mapping (payer_endpoint_id,endpoint_master_id)
        values ('${payer_endpoint_id}','${newendpoints[j]}')`

                    console.log("newendpoints mapping", sqlQuery3)
                    pool.query(sqlQuery3, (err, resultss, _fields) => {
                        if (err) throw err;
                    });
                }
                var sql = `UPDATE payer_end_points  set auth_scope ='${auth_scope}', base_url='${base_url}',
                token_url ='${token_url}', auth_type='${auth_type}', authorize_url ='${authorize_url}',
                 return_url='${return_url}' 
                where  payer_id ='${payer_id}' and payer_environment_type ='${envType}' and id =${payer_end_points_id}
                 RETURNING *`
                console.log(sql, "sql")
                pool.query(sql, (error, result) => {
                    if (error) {
                        throw error
                    }
                })

            });

            
            response.status(200).json({ body: 'successfully created a end points' })
        // })
    });
}


const userIdByEndpointCount = (request, response) => {
    const user_id = parseInt(request.params.user_id)
    var sql = `
	SELECT  payer_end_points.base_url,COUNT(*)  count_name FROM payer_end_points
LEFT JOIN payers_organisation_detail ON payer_end_points.payer_id = payers_organisation_detail.payer_id
LEFT JOIN payer_end_point_mapping ON payer_end_point_mapping.payer_endpoint_id = payer_end_points.id
LEFT JOIN end_point_master ON end_point_master.id = payer_end_point_mapping.endpoint_master_id
LEFT JOIN users ON users.user_id = payers_organisation_detail.user_id
where  payers_organisation_detail.user_id =${user_id} GROUP BY 1 `
    pool.query(sql, (error, results) => {
        if (error) {
            throw error
        }
        var data = results.rows
        response.status(200).json({ body: 'Success', data })
        console.log(data)

    })
}

module.exports = {
    userIdByEndpointCount,
    getEndPointsList,
    getEndPoints,
    createEndPoints,
    deleteEndPoints,
    updateEndPoints,
    getIdByEndPoints,
    getEndPointsAll,
    getAllEndPoints,
    getAdminEndPointsAll,
    createAdminEndPoints,
    getTypeEndPoints,
    getEndPointsUrls

}


// const updateEndPoints = (request, response) => {
//     const payer_id = parseInt(request.params.payer_id)
// console.log(payer_id, "payer_id")




//     const {  endpoint_name, base_url, auth_scope, user_id,name, other_endpoint_name, button_text, redirect_url } = request.body
//     console.log(request.body.endpoint_name,"yyyyyyyyyyyy")
//     var endpointarray = other_endpoint_name.split(',');

//     console.log(endpointarray, 'endpointarray');
//     var newendpoints = [];
//     for (var i = 0; i < endpointarray.length; i++) {
//         console.log(endpointarray[i], 'endpointarray[i]');

//         var sqlQuery4 = `INSERT INTO end_point_master(payer_id,endpoint_name)
//                         VALUES (${payer_id},'${endpointarray[i]}') RETURNING * `
//         console.log(sqlQuery4, "sqlQuery4")
//         pool.query(sqlQuery4, (error, results) => {
//             if (error) {
//                 throw error
//             }
//             newendpoints.push(results.rows[0].id);
//             console.log("Loop Started ", i)
//             console.log(" Started minus value-- ", endpointarray.length)
//             if (i == endpointarray.length) {
//                 console.log("Loop ended======== ", newendpoints)
//             }
//         })
//     }

//     const sleep = (waitTimeInMs) => new Promise(resolve => setTimeout(resolve, waitTimeInMs));

//     sleep(1000).then(() => {
//         var sql = `INSERT INTO payer_end_points(payer_id,endpoint_name,inserted_by)
//             VALUES ('${payer_id}','${endpoint_name}','${user_id}') RETURNING * `
//         console.log(sql, "sql")
//         pool.query(sql, (error, results) => {
//             if (error) {
//                 throw error
//             }
//             var payer_endpoint_id = results.rows[0].id;
//           //  console.log(payer_endpoint_id, "payer_end_points")

//             var sqlQuery2 = `select id as endpoint_master_id, endpoint_name from end_point_master`


//             pool.query(sqlQuery2, (err, result, _fields) => {
//                 if (err) throw err;
//                 var data = result.rows
//              //   console.log(data, "end_point_master")

//                 for (var i = 0; i < endpoint_name.length; i++) {
//                     console.log(endpoint_name, "endpoint_name")

//                     console.log(endpoint_name[i], "end_point_master")
//                     var sqlQuery3 = `INSERT INTO payer_end_point_mapping (payer_endpoint_id,endpoint_master_id)
//             values ('${payer_endpoint_id}','${endpoint_name[i]}')`

//                     console.log("payer_end_point_mapping", sqlQuery3)
//                     pool.query(sqlQuery3, (err, resultss, _fields) => {
//                         if (err) throw err;

//                     });

//                 }

//                 console.log(newendpoints.length, "newendpoints length")
//                 for (var j = 0; j < newendpoints.length; j++) {

//                     console.log(newendpoints[j], "newendpoints")
//                     var sqlQuery3 = `INSERT INTO payer_end_point_mapping (payer_endpoint_id,endpoint_master_id)
//             values ('${payer_endpoint_id}','${newendpoints[j]}')`

//                     console.log("newendpoints mapping", sqlQuery3)
//                     pool.query(sqlQuery3, (err, resultss, _fields) => {
//                         if (err) throw err;

//                     });

//                 }
//                 // var button_text = 'See Endpoint'
//                 // var notificationtext = 'Patient Access API Endpoints are added by administrator'
//                 // var sqlQuery4 = `INSERT INTO notifications (payer_id, user_id, button_text, redirect_url,notificationtext,name)
//                 // values ('${payer_id}','${user_id}','${button_text}','${'./Endpoints'}','${notificationtext}','${name}')`
//                 // console.log(sqlQuery4, "notifactions")
//                 // pool.query(sqlQuery4, (err, resultss, _fields) => {
//                 //     if (err) throw err;

//                 // });
//                 var sql = `UPDATE payer_end_points  set auth_scope ='${auth_scope}', base_url=
//           '${base_url}' where  payer_id ='${payer_id}' RETURNING *`
//                 console.log(sql, "sql")
//                 pool.query(sql, (error, result) => {
//                     if (error) {
//                         throw error
//                     }
//                 })
//                 var data = results.rows
//                 response.status(200).json({ body: 'successfully created a end points', data })
//             })
//         });

//     })
// }






