var pool = require('../config/db.config')

const getUserTypes = (request, response) => {
    var sql =`SELECT * FROM user_types`
    pool.query(sql, (error, results) => {
        if (error) {
            throw error
        }
        var data = results.rows
        response.status(200).json({ body: 'Success', data })
        console.log(data)

    })
}



const createUserTypes = (request, response) => {
    const { id, user_type, inserted_date } = request.body
    pool.query('INSERT INTO user_types (id, user_type, inserted_date) VALUES ($1, $2, $3)', [id, user_type, inserted_date], (error, results) => {
        if (error) {
            throw error
        }
        response.status(200).json({ body: 'successfully created a user types' })
        // console.log(data)

    })
}
const deleteUserTypes = (request, response) => {
    const id = parseInt(request.params.id)
    pool.query('DELETE FROM user_types WHERE id = $1', [id], (error, results) => {
        if (error) {
            throw error
        }
        // response.status(200).send(`User deleted with ID: ${id}`)
        response.status(200).json({ body: 'successfully deleted a user types ' })

    })
}

const updateUserTypes = (request, response) => {
    const id = parseInt(request.params.id)
    const { user_type, inserted_date } = request.body
    pool.query(
        "UPDATE user_types SET user_type = $1, inserted_date = $2 WHERE id = $3",
        [user_type, inserted_date, id],
        (error, results) => {
            if (error) {
                throw error
            }
            response.status(200).json({ body: 'successfully update a user types' })

        }
    )
}





module.exports = {
    getUserTypes,
    createUserTypes,
    updateUserTypes,
    deleteUserTypes
}










