const express = require('express')
const bodyParser = require('body-parser')
require('dotenv').config()
const app = express()
//const Authorization_Details = require('./app/controllers/user.controller')
const port = 7000

app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
)
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(bodyParser.json());
const cors = require('cors');
const corsOptions = {
  origin: '*',
  credentials: true,            //access-control-allow-credentials:true
  optionSuccessStatus: 200
}
app.use(cors(corsOptions));
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'json, html, x-www-form-urlencoded, Origin, X-Requested-With, Content-Type, Accept, Authorization, *');
  if (req.method === 'OPTIONS') {
    res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, PATCH, DELETE, OPTIONS');
    res.setHeader('Access-Control-Allow-Credentials', true);
    return res.status(200).json({});
  }
  next();
});
//  var authorize = require("./app/config/authorize");
//   app.use(authorize);

//app.use(express.static('public'));
app.use('/api/images', express.static('images'));

app.get('/images', (request, response) => {
  response.json({ info: 'Node.js, Express, and Postgres API' })
})


var routes = require("./app/routes").routes;
app.use("/api", routes);

app.get('*', (req, res) => {
  res.send({
    status_code: "404",
    message: 'API Unavailable'
  })
console.log("API Unavailable")
})
app.post('*', (req, res) => {
  res.send({
    status_code: "404",
    message: 'API Unavailable'
  })
console.log("API Unavailable")
})




app.listen(port, () => {
  console.log(`App running on port ${port}.`)
})